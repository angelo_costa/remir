const mix = require('laravel-mix');
mix.disableSuccessNotifications();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/news-page-share-buttons.js', 'public/js')
   .js('resources/js/book-page-share-buttons.js', 'public/js')
   .js('resources/js/vendor/jssocials.min.js', 'public/js')
   .js('resources/js/equipe.js', 'public/js')
   .js('resources/js/tags-nuvem.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.copy('resources/css/vendor/font-awesome.min.css', 'public/css')
mix.copy('resources/css/vendor/jssocials.css', 'public/css')
mix.copy('resources/css/vendor/jssocials-theme-plain.css', 'public/css')
