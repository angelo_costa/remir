FROM php:7.2-fpm

RUN apt-get update && apt-get install -y \
      libicu-dev \
      libpq-dev \
      libxml2-dev \
      zlib1g-dev \
      libpng-dev \
      libjpeg62-turbo-dev \
      libfreetype6-dev \
    && rm -r /var/lib/apt/lists/* \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install \
      intl \
      mbstring \
      pcntl \
      pdo \
      pdo_pgsql \
      opcache \
      dom \
      zip \
      gd

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

# install phpunit
RUN curl https://phar.phpunit.de/phpunit.phar -L > phpunit.phar \
  && chmod +x phpunit.phar \
  && mv phpunit.phar /usr/local/bin/phpunit \
  && phpunit --version

# install cron
RUN apt-get update && apt-get -y install rsyslog

# Change uid and gid of apache to docker user uid/gid
RUN usermod -u 1000 www-data && groupmod -g 1000 www-data

WORKDIR /var/www/html
