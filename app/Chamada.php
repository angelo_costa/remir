<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chamada extends Model
{
    use SoftDeletes;

    protected $table = 'chamadas';

    protected $fillable = [
        'titulo',
        'corpo',
        'noticia_id',
        'user_id',
        'secao_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function noticia() {
        return $this->hasOne('App\Noticia');
    }

    public function autor() {
        return $this->hasOne('App\User');
    }

    public function imagens() {
      return $this->belongsToMany('App\Services\Imgur\Model\Imagem');
    }
}
