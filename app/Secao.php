<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Secao extends Model
{
    use SoftDeletes;

    protected $table = 'secoes';

    protected $fillable = [
        'titulo',
        'id_interno',
        'orientacao',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function chamadas() {
        return $this->belongsToMany('App\Chamada');
    }
}
