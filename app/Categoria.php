<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model
{
    use SoftDeletes;

    protected $table = 'categorias';

    protected $fillable = [
        'nome',
        'descricao',
        'permalink',
        'slug',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function destaques() {
      return $this->belongsToMany('App\Noticia', 'categoria_destaques')->limit(2);
    }

    public function tags() {
      return $this->belongsToMany('App\Tag', 'categoria_tag');
    }
}
