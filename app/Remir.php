<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Remir extends Model
{
    protected $table = 'remir';

    protected $fillable = [
        'titulo',
        'corpo'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
