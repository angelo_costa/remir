<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Log;

class Home extends Model
{
    protected $table = 'home';

    protected $fillable = [
      'template',
    ];

    protected $dates = [
      'created_at',
      'updated_at',
    ];
}
