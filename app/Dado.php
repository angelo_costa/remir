<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dado extends Model
{
    protected $table = 'dados';

    protected $fillable = [
        'titulo',
        'link',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function tags() {
      return $this->belongsToMany('App\Tag', 'dado_tag');
    }
}
