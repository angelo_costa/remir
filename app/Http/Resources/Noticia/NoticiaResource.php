<?php

namespace App\Http\Resources\Noticia;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Categoria\CategoriaResource;
use App\Http\Resources\Noticia\NoticiaStatusResource;

class NoticiaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($noticia)
    {
        return [
            'id' => $noticia->id,
            'titulo' => $noticia->titulo,
            // 'autor' => $noticia->autor,
            'corpo' => $noticia->corpo,
            'subtitulo' => $noticia->subtitulo,
            'imagem' => $noticia->imagem,
            'local' => $noticia->local,
            'categoria' => $noticia->categoria,
            // 'status' => NoticiaStatusResource::collection($noticia->status),
            'created_at' => $noticia->created_at,
            'updated_at' => $noticia->updated_at,
            'published_at' => $noticia->published_at,
        ];
    }
}
