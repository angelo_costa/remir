<?php

namespace App\Http\Resources\Noticia;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NoticiaResourceCollection extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = 'App\Http\Resources\Noticia\NoticiaResource';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($noticias)
    { 
      info($noticias);
        return [
            'data' => $noticias,
        ];
    }
}
