<?php

namespace App\Http\Resources\Noticia;

use Illuminate\Http\Resources\Json\JsonResource;

class NoticiaStatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nome' => $this->nome,
        ];
    }
}
