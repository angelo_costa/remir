<?php

namespace App\Http\Controllers\Api\Remir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Remir;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Validator;
use Input;

class RemirController extends Controller implements GeneralApiInterface
{
    /**
     * Recupera todas os livros.
     *
     */
    public function all(Request $request) {
      $livros = (new Livro)->newQuery();

      if ($request->has('q')) {
        $q = json_decode($request->input('q'));

        if (isset($q->titulo)) {
          $livros->where('titulo', 'like', '%' . $q->titulo . '%');
          // $livros->whereRaw(" titulo LIKE '%" . $q->titulo . "%' ");
        }

        if (isset($q->autor)) {
          $livros->where('autor', 'like', '%' . $q->autor . '%');
        }


        if (isset($q->tipo)) {
          $livros->where('tipo', $q->tipo);
        }

        if (isset($q->instituicao)) {
          $livros->where('instituicao', 'like', '%' . $q->instituicao . '%')->orWhere('sigla', 'like', '%' . $q->instituicao . '%');
        }
      }

      return $livros->paginate(20);
    }


    /**
     * Recupera apenas uma categoria.
     *
     * @param int $categoriaId
     *      Id da categoria.
     *
     * @return App\Http\Resources\Categoria\CategoriaResource
     *      Retorna apenas o resource de uma categoria.
     */
    public function find() {
      return Remir::latest()->first();
    }


    /**
     * Cria uma categoria.
     *
     * @param string $nome
     *      Nome da categoria.
     *
     * @return App\Http\Resources\Categoria\CategoriaResource
     *      Retorna um resource da categoria.
     */
    public function create(Request $request) {
      $validation = Validator::make($request->all(),[
        'titulo'     => 'string|required',
        'corpo'      => 'string|required',
      ]);

      if ($validation->fails()) {
          return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
      }

      $pagina = new Remir();

      $pagina->fill($request->all());
      $pagina->save();

      return $pagina;
    }

    public function update(Request $request, int $livroId) {
      $validation = Validator::make($request->all(),[
        'titulo'     => 'string|required',
        'autor'      => 'string|required',
        'orientador' => 'string|nullable',
        'ano'        => 'integer|nullable',
        'tipo'       => 'string|nullable',
        'instituicao' => 'string|nullable',
        'sigla'       => 'string|nullable',
        'repositorio' => 'string|nullable',
        'resumo'      => 'string|nullable',
        'arquivo'     => 'string|nullable',
        'permalink' => 'string|required',
        'tag_id' => 'array|nullable',
        'slug' => 'string|required',
      ]);

      if ($validation->fails()) {
          return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
      }

      $livro = Livro::findOrFail($livroId);

      if (!empty($request->tag_id)) {
        // $tagIds = array_map('intval', explode(',' , $request->tag_id));

        // Atualiza sem deixar duplicatas
        // Sync é para relações belongsToMany
        $livro->tags()->sync($request->tag_id);
      } else {
        $livro->tags()->sync([]);
      }

      $livro->fill($request->all());
      $livro->save();

      return $livro;
  }

    /**
     * Deleta uma categoria.
     *
     * @param int $categoriaId
     *      O id da categoria.
     */
    public function delete(int $livroId) {
        $livro = Livro::find($livroId);

        $livro->delete();

        return response()->json([
            'livros' => Livro::all()
        ], GeneralApiInterface::HTTP_200);
    }
}
