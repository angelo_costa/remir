<?php

namespace App\Http\Controllers\Api\Home;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Home;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Validator;
use Input;
use Log;

class HomeController extends Controller implements GeneralApiInterface
{
    public function find() {
      return Home::orderBy('created_at', 'DESC')->first();
    }

    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
            'template' => 'required|string',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $home = Home::create([
            'template' => $request->template,
        ]);

        return $home;
    }
}
