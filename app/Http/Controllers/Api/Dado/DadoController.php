<?php

namespace App\Http\Controllers\Api\Dado;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Dado;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Validator;
use Input;
use Log;

class DadoController extends Controller implements GeneralApiInterface
{
    /**
     * Recupera todas os livros.
     *
     */
    public function all(Request $request) {
      $dados = (new Dado)->newQuery()->orderBy('updated_at', 'DESC');

      return $dados->paginate(20);
    }

    /**
     * Recupera apenas uma categoria.
     *
     * @param int $categoriaId
     *      Id da categoria.
     *
     * @return App\Http\Resources\Categoria\CategoriaResource
     *      Retorna apenas o resource de uma categoria.
     */
    public function find(int $dadoId) {
      return Dado::with('tags')->find($dadoId);
    }


    /**
     * Cria uma categoria.
     *
     * @param string $nome
     *      Nome da categoria.
     *
     * @return App\Http\Resources\Categoria\CategoriaResource
     *      Retorna um resource da categoria.
     */
    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
          'titulo'     => 'string|required',
          'link'      => 'string|required',
          'tag_id'=> 'array|nullable',
        ]);

        if ($validation->fails()) {
          return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $dado = new Dado();

        $dado->fill($request->all());
        $dado->save();

        if (!empty($request->tag_id)) {
          // $tagIds = array_map('intval', explode(',' , $request->tag_id));

          // Atualiza sem deixar duplicatas
          // Sync é para relações belongsToMany
          $dado->tags()->sync($request->tag_id);
        } else {
          $dado->tags()->sync([]);
        }

        return $dado;
    }

    public function update(Request $request, int $dadoId) {
      $validation = Validator::make($request->all(),[
        'titulo'     => 'string|required',
        'link'      => 'string|required',
        'tag_id' => 'array|nullable',
      ]);

      if ($validation->fails()) {
        return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
      }

      $dado = Dado::findOrFail($dadoId);

      if (!empty($request->tag_id)) {
        // $tagIds = array_map('intval', explode(',' , $request->tag_id));

        // Atualiza sem deixar duplicatas
        // Sync é para relações belongsToMany
        $dado->tags()->sync($request->tag_id);
      } else {
        $dado->tags()->sync([]);
      }

      $dado->fill($request->all());
      $dado->save();

      return $dado;
  }

    /**
     * Deleta uma categoria.
     *
     * @param int $categoriaId
     *      O id da categoria.
     */
    public function delete(int $dadoId) {
        $dado = Dado::find($dadoId);

        $dado->delete();

        return response()->json([
            'dados' => Dado::paginate(20)
        ], GeneralApiInterface::HTTP_200);
    }
}
