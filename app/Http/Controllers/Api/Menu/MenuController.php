<?php

namespace App\Http\Controllers\Api\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Menu\MenuResourceCollection;
use App\Http\Resources\Menu\MenuResource;
use App\Menu;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Validator;
class MenuController extends Controller implements GeneralApiInterface
{
    /**
     * Recupera todos as menus.
     *
     * @return App\Http\Resources\Menu\MenuResourceCollection
     *      Retorna a coleção de menu.
     */
    public function all() {
        return Menu::orderBy('ordem', 'ASC')->get();
    }

    /**
     * Recupera apenas uma categoria.
     *
     * @param int $menuId
     *      Id do menu.
     *
     * @return App\Http\Resources\Menu\MenuResource
     *      Retorna apenas o resource de um menu.
     */
    public function find(int $menuId) {
      return Menu::find($menuId);
    }


    /**
     * Cria um menu.
     *
     * @param string $nome
     *      Nome do menu
     *
     * @return App\Http\Resources\Menu\MenuResource]
     *      Retorna um resource menu.
     */
    public function create(Request $request) {
        $validation = Validator::make($request->all(), [
          'titulo' => 'required|string',
          'permalink' => 'required|string|unique:menus',
        ]);

        if ($validation->fails()) {
          return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $menu = Menu::create([
            'titulo' => $request->titulo,
            'permalink' => $request->permalink
        ]);

        return $menu;
    }

    /**
     * Atualiza um menu.
     *
     * @param string $nome
     *      Nome do menu.
     * @param int $menuId
     *      Id do menu.
     *
     * @return App\Http\Resources\Menu\MenuResource]
     *      Retorna um resource menu.
     */

    public function updateOrder(Request $request) {
      foreach($request->get('menu_ids') as $index => $id) {
        $item = Menu::find($id);
        $item->ordem = $request->get('ordems')[$index];
        $item->save();
      }

      return Menu::all();
    }

    public function update(Request $request, int $menuId) {
        $validation = Validator::make($request->all(), [
          'titulo' => 'required|string',
          'permalink' => 'required|string',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $menu = Menu::findOrFail($menuId);

        $menu->fill($request->all());
        $menu->save();

        return $menu;
    }


    /**
     * Atualiza um menu.
     *
     * @param int $menuId
     *      Id do menu.
     *
     * @return App\Http\Resources\Menu\MenuResource]
     *      Retorna um resource menu.
     */
    public function delete(int $menuId) {
        $menu = Menu::findOrFail($menuId);

        $menu->delete();

        return response()->json([
            'message' => 'Menu deleted witth success'
        ], GeneralApiInterface::HTTP_200);
    }

}
