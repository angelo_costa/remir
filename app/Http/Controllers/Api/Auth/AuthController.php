<?php

namespace App\Http\Controllers\Api\Auth;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Validator;

class AuthController extends Controller implements GeneralApiInterface
{
    /**
     * Cria um usuario
     *
     * @param Illuminate\Http\Request $request
     *    Request do post para a criação do usuario.
     *
     * @return string
     *    Retorna um status HTTP
     */
    public function signup(Request $request) {
        $validation = Validator::make($request->all(),[
            'nome' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string',
            'rg' => 'nullable|string|unique:users',
            'cpf' => 'nullable|string|unique:users',
            'login' => 'nullable|string|unique:users',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $user = User::create([
            'nome' => $request->nome,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'rg' => $request->rg ? $request->rg : NULL,
            'cpf' => $request->cpf ? $request->cpf : NULL,
            'login' => $request->login ? $request->login : NULL,
        ]);

        if ($user) {
            return response()->json([
                'message' => 'Successfully created user',
            ], GeneralApiInterface::HTTP_200);
        }
    }

    /**
     * Login do usuario e a criação do token.
     *
     * @param Illuminate\Http\Request $request
     *    Requests com as informações.
     *
     * @return App\User
     *    Retorna o usuario logado com o token.
     */
    public function login(Request $request) {
        $validation = Validator::make($request->all(),[
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        // Recupera as keys do request.
        // Nesse caso, tenta fazer o login para o usuario.
        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized',
            ], GeneralApiInterface::HTTP_401);
        }

        // Recupera o usuario que realizou o request.
        $user = $request->user();

        // Criar um token para acesso pessoal.
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        // expira o token em 1 semana.
        if ($request->remeber_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'user' => $user
            ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Logout.
     *
     * @param Illuminate\Http\Request $request
     *    Requests com as informações.
     *
     * @return string
     *    Retorna a mensagem de logout.
     */
    public function logout(Request $request) {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ], GeneralApiInterface::HTTP_200);
    }

    /**
     * Recupera o usuario logado.
     *
     * @param Illuminate\Http\Request $request
     *    Requests com as informações.
     */
    public function user(Request $request) {
        return response()->json($request->user(), GeneralApiInterface::HTTP_200);
    }

}
