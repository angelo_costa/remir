<?php

namespace App\Http\Controllers\Api\Secao;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Secao;
use App\Chamada;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class SecaoController extends Controller implements GeneralApiInterface
{
    /**
     * Recupera todas as seções.
     *
     * @return App\Secao
     *      Retorna a coleção de seções.
     */
    public function all() {
        return Secao::with('chamadas')->paginate(20);
    }

    /**
     * Recupera apenas uma noticia.
     *
     * @param int $secaoId
     *      Id da seção.
     *
     * @return App\Secao
     *      Retorna apenas o resource de uma secao.
     */
    public function find(int $secaoId) {
        return Secao::with('chamadas')->find($secaoId);
    }

    /**
     * Cria uma seção.
     *
     * @param string $nivel
     *      Nivel de destaque da seção.
     * @param string $autorId
     *      Autor do autor da chamada.
     *
     * @return App\Secao
     *      Retorna um resource da secao.
     */
    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
            'titulo' => 'required|string',
            'id_interno' => 'string|nullable|unique:secoes',
            'orientacao' => 'string|nullable',
            'chamada' => 'array|nullable',
            'chamada.*' => 'int',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $secao = Secao::create([
            'titulo' => $request->titulo,
            'id_interno' => $request->id_interno,
            'orientacao' => $request->orientacao,
        ]);

        if (!empty($request->chamada)) {
            foreach ($request->chamada as $chamada) {
                $this->associaChamada($secao, $chamada);
            }

            $secao->load('chamadas');
        }

        return $secao;
    }

    /**
     * Atualiza uma seção.
     *
     * @param string $nivel
     *      Nivel de destaque da seção.
     * @param string $autorId
     *      Autor do autor da chamada.
     *
     * @return App\Secao
     *      Retorna um resource da secao.
     */
    public function update(Request $request, int $secaoId) {
        $validation = Validator::make($request->all(),[
            'titulo' => 'required|string',
            'id_interno' => 'string|nullable|unique:secoes',
            'orientacao' => 'string|nullable',
            'chamada' => 'array|nullable',
            'chamada.*' => 'int',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $secao = Secao::findOrFail($secaoId);

        if (!empty($request->chamada)) {
            $this->atualizaChamada($secao, $request->chamada);           
            $secao->load('chamadas');
        }

        $secao->titulo = $request->titulo;
        $secao->id_interno = $request->id_interno;
        $secao->orientacao = $request->orientacao;
    
        return $secao;
    }

    /**
     * Deleta uma seção.
     *
     * @param int $secaoId
     *      Id da seção.
     */
    public function delete(int $secaoId) {
        $secao = Secao::findOrFail($secaoId);

        $secao->delete();

        return response()->json([
            'message' => 'Seção deleted witth success'
        ], GeneralApiInterface::HTTP_200);
    }

    /**
     * Associa a chamada.
     */
    protected function associaChamada(Secao $secao, int $chamadaId) {
        try {
            $chamada = Chamada::findOrFail($chamadaId);
            $secao->chamadas()->attach($chamada->id);
            $secao->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('User not found', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Associa a chamada.
     */
    protected function atualizaChamada(Secao $secao, array $chamadas) {
        try {
            $secao->chamadas()->sync($chamadas);
            $secao->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('User not found', GeneralApiInterface::HTTP_401);
        }
    }
}
