<?php

namespace App\Http\Controllers\Api\Equipe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Equipe;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Validator;
use Input;
use Log;

class EquipeController extends Controller implements GeneralApiInterface
{
    public function all(Request $request) {
      return Equipe::all();
    }

    public function find(int $id) {
      return Equipe::with('imagem')->find($id);
    }

    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
            'nome'   => 'string|required',
            'email'  => 'string|required',
            'lat'    => 'string|required',
            'lng'    => 'required',
            'local'  => 'required',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $equipe = new Equipe();

        $equipe->fill($request->all());
        $equipe->save();

        return $equipe;
    }

    public function update(Request $request, int $id) {
      $validation = Validator::make($request->all(),[
        'nome'   => 'string|required',
        'email'  => 'string|required',
        'lat'    => 'string|required',
        'lng'    => 'required',
        'local'  => 'required',
    ] );

      if ($validation->fails()) {
        return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
      }

      $membro = Equipe::findOrFail($id);
      $membro->fill($request->all());
      $membro->save();

      return $membro;
  }

    public function delete(int $id) {
        $membro = Equipe::findOrFail($id);

        $membro->delete();

        return response()->json([
            'equipe' => Equipe::all()
        ], GeneralApiInterface::HTTP_200);
    }
}
