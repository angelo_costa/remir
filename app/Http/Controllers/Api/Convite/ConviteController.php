<?php

namespace App\Http\Controllers\Api\Convite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Convite;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use App\Notifications\Api\ConviteRequest;
use App\Notifications\Api\ConviteSuccess;
use Carbon\Carbon;
use Validator;
Use Notification;

class ConviteController extends Controller
{
    /**
     * Cria a requisição de resetar a senha.
     *
     * @param string $email
     *      Email do usuario para resetar a senha.
     */
    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
            'email' => 'required|string|email',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $convite = Convite::updateOrCreate(
            ['email' => $request->email],
            [
                'email' => $request->email,
                'token' => str_random(60)
            ]
        );

        if ($convite) {
            Notification::route('mail', $request->email)
                ->notify(new ConviteRequest($convite->token));
        }

        return response()->json([
            'message' => 'O convite foi enviado no email',
        ], GeneralApiInterface::HTTP_200);

    }

    /**
     * Recupera o token.
     *
     * @param string $token
     *      Token enviado para o email
     */
    public function find(Request $request, $token) {

        $convite = Convite::where('token', $token)
            ->first();

        if (!$convite) {
            return response()->json([
                'message' => 'Token invalido.'
            ], GeneralApiInterface::HTTP_404);
        }

        if (Carbon::parse($convite->updated_at)->addMinutes(720)->isPast()) {
            $convite->delete();

            return response()->json([
                'message' => 'Token é invalido'
            ], GeneralApiInterface::HTTP_404);
        }

        return response()->json($convite);
    }
}
