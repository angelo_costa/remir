<?php

namespace App\Http\Controllers\Api\Noticia;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Http\Resources\Noticia\NoticiaResourceCollection;
use App\Http\Resources\Noticia\NoticiaResource;
use App\Noticia;
use App\NoticiaStatus;
use App\Categoria;
use App\Tag;
use App\Imagem;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use Log;

class NoticiaController extends Controller implements GeneralApiInterface
{
    /**
     * Recupera todas as notícias.
     *
     * @return App\Noticia
     *      Retorna a coleção de noticia.
     */
    public function all(Request $request) {
      $noticias = (new Noticia)->newQuery();


      if ($request->has('q')) {
        $q = json_decode($request->input('q'));


        if (isset($q->titulo)) {
          $noticias->where('titulo', 'like', '%' . $q->titulo . '%');
        }

        if (isset($q->subtitulo)) {
          $noticias->where('subtitulo', 'like', '%' . $q->subtitulo . '%');
        }

        if (isset($q->categoria_id)) {
          $noticias->where('categoria_id', $q->categoria_id);
        }

        if (isset($q->noticia_status_id)) {
          $noticias->where('noticia_status_id', $q->noticia_status_id);
        }
      }

      return $noticias->with('status', 'categoria', 'imagens')->orderBy('created_at', 'DESC')->paginate(20);
    }

    /**
     * Recupera apenas uma noticia.
     *
     * @param int $noticiaid
     *      Id da noticia.
     *
     * @return App\Noticia
     *      Retorna apenas o resource de uma noticia.
     */
    public function find(int $noticiaId) {
        return Noticia::with('categoria','status', 'imagens', 'tags')->find($noticiaId);
    }

    /**
     * Cria uma noticia.
     *
     * @param string $titulo
     *      Titulo da noticia.
     * @param string $autor
     *      Autor da noticia.
     * @param string $corpo
     *      Corpo da noticia.
     * @param string $subtitulo
     *      Subtitulo da noticia.
     * @param string $imagem
     *      Imagem da noticia.
     * @param string $local
     *      Local da noticia.
     * @param int $status
     *      Status da noticia.
     *
     * @return App\Http\Resources\Noticia\NoticiaResource
     *      Retorna um resource da noticia.
     */
    public function create(Request $request) {

        $validation = Validator::make($request->all(),[
            'titulo' => 'required|string',
            'corpo' => 'required|string|nullable',
            'subtitulo' => 'string|nullable',
            'imagem_id' => 'nullable',
            'local' => 'string|nullable',
            'noticia_status_id' => 'integer|nullable',
            'categoria_id' => 'integer|nullable',
            'user_id' => 'integer|nullable',
            'permalink' => 'required|string',
            'tag_id' => 'nullable',
            'slug' => 'string|required|unique:noticias',
            'autor' => 'string|required',
            'video_destaque' => 'nullable',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $noticia = new Noticia;

        $noticia->fill([
          'titulo' => $request->titulo,
          'subtitulo' => $request->subtitulo,
          'autor' => $request->autor,
          'corpo' => $request->corpo,
          'slug' => $request->slug,
          'permalink' => $request->permalink,
          'video_destaque' => $request->video_destaque ?: '',
        ]);

        if (!empty($request->noticia_status_id)) {
            $this->associaNoticiaStatus($noticia, $request->noticia_status_id);
        }

        if (!empty($request->categoria_id)) {
            $this->associaCategoria($noticia, $request->categoria_id);
        }

        if (!empty($request->user_id)) {
            $this->associaAutor($noticia, $request->user_id);
        }

        if (!empty($request->tag_id)) {
          $tagIds = array_map('intval', explode(',' , $request->tag_id));
          $noticia->tags()->saveMany(Tag::find($tagIds));
        }

        $noticia->save();

        if ((int) $request->imagem_id) {
          $noticia->imagens()->sync([$request->get('imagem_id')]);
        }

        return $noticia;
    }

    /**
     * Cria uma noticia.
     *
     * @param int $noticiaId
     *      Id da noticia
     * @param string $titulo
     *      Titulo da noticia.
     * @param string $autor
     *      Autor da noticia.
     * @param string $corpo
     *      Corpo da noticia.
     * @param string $subtitulo
     *      Subtitulo da noticia.
     * @param string $imagem
     *      Imagem da noticia.
     * @param string $local
     *      Local da noticia.
     * @param int $status
     *      Status da noticia.
     *
     * @return App\Http\Resources\Noticia\NoticiaResource
     *      Retorna um resource da noticia.
     */
    public function update(Request $request, int $noticiaId) {
      Log::info($request->autor);
        $validation = Validator::make($request->all(),[
            'titulo' => 'required|string',
            'corpo' => 'string|nullable',
            'subtitulo' => 'string|nullable',
            'imagem_id' => 'nullable',
            'local' => 'string|nullable',
            'noticia_status_id' => 'integer|nullable',
            'categoria_id' => 'integer|nullable',
            'tag_id' => 'string|nullable',
            'user_id' => 'nullable',
            'permalink' => 'required',
            'autor' => 'string|required',
            'video_destaque' => 'nullable',
            'slug' => [
              'required',
              Rule::unique('noticias')->ignore($noticiaId)->whereNull('deleted_at')
            ]
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $noticia = Noticia::findOrFail($noticiaId);

        if (!empty($request->noticia_status_id)) {
            $this->associaNoticiaStatus($noticia, $request->noticia_status_id);
        }

        if (!empty($request->categoria_id)) {
            $this->associaCategoria($noticia, $request->categoria_id);
        }

        if ((int) $request->get('imagem_id')) {
          $noticia->imagens()->sync([$request->get('imagem_id')]);
        } else {
          $noticia->imagens()->sync([]);
        }

        if (!empty($request->tag_id)) {
          $tagIds = array_map('intval', explode(',' , $request->tag_id));

          $noticia->tags()->sync($tagIds);
        } else {
          $noticia->tags()->sync([]);
        }

        $noticia->fill([
          'titulo' => $request->titulo,
          'subtitulo' => $request->subtitulo,
          'video_destaque' => $request->video_destaque ?: '',
          'autor' => $request->autor,
          'corpo' => $request->corpo,
          'slug' => $request->slug,
          'permalink' => $request->permalink,
        ]);

        $noticia->save();

        return $noticia;
    }

    /**
     * Deleta uma noticia.
     *
     * @param int $noticiaId
     *      Id da noticia.
     */
    public function delete(int $noticiaId) {
        $noticia = Noticia::findOrFail($noticiaId);

        $noticia->delete();

        return response()->json([
            'noticias' => 'Noticia deleted witth success'
        ], GeneralApiInterface::HTTP_200);
    }

    /**
     * Associa o status da noticia.
     */
    protected function associaNoticiaStatus(Noticia $noticia, int $noticia_status_id) {
        try {
            $status = NoticiaStatus::findOrFail($noticia_status_id);
            $noticia->status()->associate($status->id);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Status not found', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Associa noticia com uma categoria.
     */
    protected function associaCategoria(Noticia $noticia, int $categoria_id) {
        try {
            $categoria = Categoria::findOrFail($categoria_id);
            $noticia->categoria()->associate($categoria->id);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Categoria not found', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Associa noticia com um autor.
     */
    protected function associaAutor(Noticia $noticia, int $user_id) {
        try {
            $user = User::findOrFail($user_id);
            $noticia->categoria()->associate($user->id);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Autor not found', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Associa noticia com uma imagem.
     */
    protected function associaImagem(Noticia $noticia, Imagem $imagem) {
        try {
            $noticia->imagens()->attach($imagem->id);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Error in associate imagem', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Atualiza noticia com uma nova imagem.
     */
    protected function atualizaImagem(Noticia $noticia, array $imagens) {
        try {
            $noticia->imagens()->sync($imagens);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Error in associate imagem', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Associa noticia com uma tag.
     */
    protected function associaTag(Noticia $noticia, int $tag_id) {
        try {
            $tag = Tag::findOrFail($tag_id);
            $noticia->tags()->attach($tag->id);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Tag not found', GeneralApiInterface::HTTP_401);
        }
    }
}
