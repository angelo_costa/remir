<?php

namespace App\Http\Controllers\Api\Categoria;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Categoria;
use App\Noticia;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Validator;
use Input;
use Log;

class CategoriaController extends Controller implements GeneralApiInterface
{
    /**
     * Recupera todas as categorias.
     *
     * @return App\Http\Resources\Categoria\CategoriaResourceCollection
     *      Retorna a coleção de categoria.
     */
    public function all() {
        return Categoria::with('destaques')->orderBy('nome', 'ASC')->paginate(20);
    }

    /**
     * Recupera apenas uma categoria.
     *
     * @param int $categoriaId
     *      Id da categoria.
     *
     * @return App\Http\Resources\Categoria\CategoriaResource
     *      Retorna apenas o resource de uma categoria.
     */
    public function find(int $categoriaId) {
      return Categoria::with('destaques.imagens', 'destaques.categoria', 'tags')->find($categoriaId);
    }

    /**
     * Recebe a categoria e um array de noticia_ids
     *
     */
    public function saveDestaques(int $categoriaId, Request $request) {
      $categoria = Categoria::findOrFail($categoriaId);

      if (!empty($request->get('destaques'))) {
        $categoria->destaques()->sync(Noticia::find($request->get('destaques')));
      }
      else {
        $categoria->destaques()->sync([]);
      }

      $categoria->save();
      return $categoria;
    }

    /**
     * Cria uma categoria.
     *
     * @param string $nome
     *      Nome da categoria.
     *
     * @return App\Http\Resources\Categoria\CategoriaResource
     *      Retorna um resource da categoria.
     */
    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
            'nome' => 'required|string',
            'permalink' => 'string|required',
            'descricao' => 'string|nullable',
            'slug' => 'string|required|unique:categorias',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $categoria = Categoria::create([
            'nome' => $request->nome,
            'descricao' => $request->descricao,
            'permalink' => $request->permalink,
            'slug' => $request->slug
        ]);

        if (!empty($request->tag_id)) {
          // $tagIds = array_map('intval', explode(',' , $request->tag_id));

          // Atualiza sem deixar duplicatas
          // Sync é para relações belongsToMany
          $categoria->tags()->sync($request->tag_id);
        } else {
          $categoria->tags()->sync([]);
        }


        return $categoria;
    }

    /**
     * Atualiza uma categoria.
     *
     * @param int $categoriaId
     *      O id da categoria a ser atualizada.
     * @param string $nome
     *      Nome da categoria.
     *
     * @return App\Http\Resources\Categoria\CategoriaResource
     *      Retorna um resource da categoria.
     */
    public function update(Request $request, int $categoriaId) {
        $validation = Validator::make($request->all(),[
          'nome' => 'required|string',
          'permalink' => 'string|required',
          'descricao' => 'string|nullable',
          'tag_id' => 'array|nullable',
          'slug' => [
            'required',
            Rule::unique('categorias')->ignore($categoriaId)
          ]
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $categoria = Categoria::findOrFail($categoriaId);
        $categoria->fill($request->all());


        if (!empty($request->tag_id)) {
          // $tagIds = array_map('intval', explode(',' , $request->tag_id));

          // Atualiza sem deixar duplicatas
          // Sync é para relações belongsToMany
          $categoria->tags()->sync($request->tag_id);
        } else {
          $categoria->tags()->sync([]);
        }

        $categoria->save();

        return $categoria;
    }

    /**
     * Deleta uma categoria.
     *
     * @param int $categoriaId
     *      O id da categoria.
     */
    public function delete(int $categoriaId) {
        $categoria = Categoria::find($categoriaId);

        $categoria->delete();

        return response()->json([
            'message' => 'Category deleted witth success'
        ], GeneralApiInterface::HTTP_200);
    }
}
