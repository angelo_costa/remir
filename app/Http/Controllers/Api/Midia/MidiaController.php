<?php

namespace App\Http\Controllers\Api\Midia;

use App\Imagem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Validator;

class MidiaController extends Controller {

    public function all(Request $request) {
      return Imagem::orderBy('created_at', 'DESC')->paginate(8);
    }

    public function find(int $imagemId) {
        return Imagem::find($imagemId);
    }

    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
            'titulo' => 'required|string',
            'legenda' => 'string|nullable',
            'creditos' => 'string|nullable',
            'cloudnary' => 'required|string'
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $imagem = new Imagem;
        $imagem->fill($request->all());

        $imagem->save();

        return $imagem;
    }

    public function update(Request $request, int $imagemId) {
        $validation = Validator::make($request->all(),[
          'titulo' => 'required|string',
          'legenda' => 'string|nullable',
          'creditos' => 'string|nullable',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $imagem = Imagem::findOrFail($imagemId);

        $imagem->fill($request->all());
        $imagem->save();

        return $imagem;
    }

    /**
     * Deleta uma mídia.
     *
     * @param int $midiaId
     *      Id da mídia.
     */
    public function delete(int $midiaId) {
        $midia = Imagem::findOrFail($midiaId);

        $midia->delete();

        return response()->json([
            'message' => 'Mídia deleted witth success'
        ], GeneralApiInterface::HTTP_200);
    }

    /**
     * Associa o status da noticia.
     */
    protected function associaNoticiaStatus(Noticia $noticia, int $noticia_status_id) {
        try {
            $status = NoticiaStatus::findOrFail($noticia_status_id);
            $noticia->status()->associate($status->id);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Status not found', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Associa noticia com uma categoria.
     */
    protected function associaCategoria(Noticia $noticia, int $categoria_id) {
        try {
            $categoria = Categoria::findOrFail($categoria_id);
            $noticia->categoria()->associate($categoria->id);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Categoria not found', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Associa noticia com um autor.
     */
    protected function associaAutor(Noticia $noticia, int $user_id) {
        try {
            $user = User::findOrFail($user_id);
            $noticia->categoria()->associate($user->id);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Autor not found', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Associa noticia com uma imagem.
     */
    protected function associaImagem(Noticia $noticia, Imagem $imagem) {
        try {
            $noticia->imagens()->attach($imagem->id);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Error in associate imagem', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Atualiza noticia com uma nova imagem.
     */
    protected function atualizaImagem(Noticia $noticia, array $imagens) {
        try {
            $noticia->imagens()->sync($imagens);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Error in associate imagem', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Associa noticia com uma tag.
     */
    protected function associaTag(Noticia $noticia, int $tag_id) {
        try {
            $tag = Tag::findOrFail($tag_id);
            $noticia->tags()->attach($tag->id);
            $noticia->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Tag not found', GeneralApiInterface::HTTP_401);
        }
    }
}
