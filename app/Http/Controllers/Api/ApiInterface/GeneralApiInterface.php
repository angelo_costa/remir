<?php

namespace App\Http\Controllers\Api\ApiInterface;

/**
 * Interface geral para os controllers da api.
 */
interface GeneralApiInterface {
  /**
   * Constante para http 200.
   */
  const HTTP_200 = 200;

  /**
   * Constante para http 404.
   */
  const HTTP_404 = 404;

  /**
   * Constante para http 401.
   */
  const HTTP_401 = 401;

  /**
   * Contante para http 500.
   */
  const HTTP_500 = 500;

}