<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\User;
use App\Convite;
use Validator;
use Input;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;

class UserController extends Controller implements GeneralApiInterface
{
    public function all() {
        return User::all();
    }

    public function create(Request $request, $token) {
      $convite = Convite::where('token', $token)->first();

      if ($convite) {
        $validation = Validator::make($request->all(),[
          'nome' => 'required|string',
          'sobrenome' => 'required|string',
          'email' => 'required|string',
          'password' => 'required|string'
        ]);


        if ($validation->fails()) {
          return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $user = new User();

        $user->fill([
          'nome' => $request->nome,
          'sobrenome' => $request->sobrenome,
          'email' => $request->email,
          'password' => bcrypt($request->password),
        ]);

        $user->save();

        if ($user) {
          $convite->delete();

          return response()->json([
              'message' => 'Successfully created user',
          ], GeneralApiInterface::HTTP_200);
        }
      }
    }
}
