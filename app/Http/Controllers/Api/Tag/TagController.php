<?php

namespace App\Http\Controllers\Api\Tag;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Tag;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Validator;
use Input;
use Log;

class TagController extends Controller implements GeneralApiInterface
{
    /**
     * Recupera todas as tags.
     *
     * @return App\Tag
     *      Retorna a coleção de tags.
     */
    public function all() {
        return Tag::all();
    }

    /**
     * Recupera apenas uma tag.
     *
     * @param int $tagId
     *      Id da tag.
     *
     * @return App\Tag
     *      Retorna apenas uma tag.
     */
    public function find(int $tagId) {
      return Tag::find($tagId);
    }

    /**
     * Cria uma tag.
     *
     * @param string $nome
     *      Nome da Tag.
     *
     * @return App\Tag
     *      Retorna uma tag.
     */
    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
            'nome' => 'required|string|unique:tags',
            'permalink' => 'required|string',
            'slug' => 'required|string'
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $tag = Tag::create([
            'nome' => $request->nome,
            'permalink' => $request->permalink,
            'slug' => $request->slug
        ]);

        return $tag;
    }

    /**
     * Atualiza uma tag.
     *
     * @param int $tagId
     *      O id da tag a ser atualizada.
     * @param string $nome
     *      Nome da tag.
     *
     * @return App\Tag
     *      Retorna uma tag.
     */
    public function update(Request $request, int $tagId) {
        $tag = Tag::findOrFail($tagId);

        $validation = Validator::make($request->all(),[
          'nome' => 'required|string',
          'permalink' => [
            'required',
            Rule::unique('tags')->ignore($tagId)
          ]
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $tag->fill($request->all());
        $tag->save();

        return $tag;
    }

    /**
     * Deleta uma tag.
     *
     * @param int $tagId
     *      O id da tag.
     */
    public function delete(int $tagId) {
        $tag = Tag::find($tagId);

        $tag->delete();

        return response()->json([
            'message' => 'Tag deleted witth success'
        ], GeneralApiInterface::HTTP_200);
    }

    public function nuvem() {
      return Tag::withCount('noticias')->orderBy('noticias_count', 'DESC')->take(30)->get();
    }
}
