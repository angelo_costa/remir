<?php

namespace App\Http\Controllers\Api\Password;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\Api\PasswordResetRequest;
use App\Notifications\Api\PasswordResetSuccess;
use App\User;
use App\PasswordReset;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Validator;
use Carbon\Carbon;

class PasswordResetController extends Controller implements GeneralApiInterface
{
    
    /**
     * Cria a requisição de resetar a senha.
     * 
     * @param string $email
     *      Email do usuario para resetar a senha.
     */
    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
            'email' => 'required|string|email',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json([
                'message' => 'E-mail não encontrado',
            ], GeneralApiInterface::HTTP_404);
        }

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => str_random(60)
            ]
        );

        if ($user && $passwordReset) {
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
        }

        return response()->json([
            'message' => 'O link de resetar a senha foi enviado no email',
        ], GeneralApiInterface::HTTP_200);

    }

    /**
     * Recupera o token.
     *
     * @param string $token
     *      Token enviado para o email
     */
    public function find(Request $request) {
        $validation = Validator::make($request->all(),[
            'token' => 'required|string',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $passwordReset = PasswordReset::where('token', $request->token)
            ->first();

        if (!$passwordReset) {
            return response()->json([
                'message' => 'Token invalido.'
            ], GeneralApiInterface::HTTP_404);
        }
    
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();

            return response()->json([
                'message' => 'Token é invalido'
            ], GeneralApiInterface::HTTP_404);
        }

        return response()->json($passwordReset);
    }

     /**
     * Reset password
     *
     * @param string email
     *      Email que terá a senha atualizada.
     * @param string password
     *      Nova senha.
     * @param string password_confirmation
     *      A confirmação da nova senha.
     * @param string token
     *      Token gerado para o reset da senha.
     *
     * @return User
     *      Retorna o usuario que solicitou a atualização de senha
     */
    public function reset(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);
        
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();
        
        if (!$passwordReset) {
            return response()->json([
                'message' => 'Token é invalido.'
            ], GeneralApiInterface::HTTP_401);
        }
        
            $user = User::where('email', $passwordReset->email)->first();
        
        if (!$user) {
            return response()->json([
                'message' => 'Não foi encontrado nenhum email cadastrado.'
            ], GeneralApiInterface::HTTP_401);
        }
        
        $user->password = bcrypt($request->password);
        $user->save();

        $passwordReset->delete();

        $user->notify(new PasswordResetSuccess($passwordReset));
        return $user;
    }

}
