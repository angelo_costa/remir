<?php

namespace App\Http\Controllers\Api\Chamada;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Chamada;
use App\Noticia;
use App\User;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class ChamadaController extends Controller implements GeneralApiInterface
{
    /**
     * Recupera todas as chamadas.
     *
     * @return App\Chamada
     *      Retorna a coleção de chamada.
     */
    public function all() {
        return Chamada::with('noticia')->paginate(20);
    }

    /**
     * Recupera apenas uma chamada.
     *
     * @param int $chamadaId
     *      Id da chamada.
     *
     * @return App\Chamada
     *      Retorna apenas o resource de uma chamada.
     */
    public function find(int $chamadaId) {
        return Chamada::with('noticia')->find($chamadaId);
    }

    /**
     * Cria uma chamada.
     *
     * @param string $titulo
     *      Titulo da chamada.
     * @param int $noticiaId
     *      Id da noticia.
     *
     * @return App\Http\Resources\Noticia\NoticiaResource
     *      Retorna um resource da noticia.
     */
    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
            'titulo' => 'required|string',
            'noticia_id' => 'integer|nullable',
            'user_id' => 'integer|nullable',
            'corpo' => 'string|nullable',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $chamada = Chamada::create([
            'titulo' => $request->titulo,
            'corpo' => $request->corpo,
        ]);

        if (!empty($request->noticia_id)) {
            $this->associaNoticia($chamada, $request->noticia_id);
        }

        if (!empty($request->user_id)) {
            $this->associaAutor($chamada, $request->user_id); 
        }

        return $chamada;
    }

    /**
     * Cria uma chamada.
     *
     * @param int $chamadaId
     *      Id da chamda.
     * @param string $titulo
     *      Titulo da chamada.
     * @param int $noticiaId
     *      Id da noticia.
     *
     * @return App\Http\Resources\Noticia\NoticiaResource
     *      Retorna um resource da noticia.
     */
    public function update(Request $request, int $chamadaId) {
        $validation = Validator::make($request->all(),[
            'titulo' => 'required|string',
            'noticia_id' => 'integer|nullable',
            'user_id' => 'integer|nullable',
            'corpo' => 'string|nullable',
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), GeneralApiInterface::HTTP_401);
        }

        $chamada = Chamada::findOrFail($chamadaId);
        
        if (!empty($request->noticia_id)) {
            $this->associaNoticia($chamada, $request->noticia_id);
        }

        if (!empty($request->user_id)) {
            $this->associaAutor($chamada, $request->user_id); 
        }

        $chamada->titulo = $request->titulo;
        $chamada->corpo = $request->corpo;

        $chamada->save();

        return $chamada;
    }

    /**
     * Deleta uma chamada.
     *
     * @param int $chamadaId
     *      Id da chamada.
     */
    public function delete(int $chamadaId) {
        $chamada = Chamada::findOrFail($chamadaId);

        $chamada->delete();

        return response()->json([
            'message' => 'Chamada deleted witth success'
        ], GeneralApiInterface::HTTP_200);
    }

    /**
     * Faz a associação com o model de noticia.
     */
    protected function associaNoticia(Chamada $chamada, int $noticia_id) {
        try {
            $noticia = Noticia::findOrFail($noticia_id);
            $chamada->noticia()->associate($noticia->id);
            return $chamada->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('Noticia not found', GeneralApiInterface::HTTP_401);
        }
    }

    /**
     * Faz a associação com o model de user.
     */
    protected function associaAutor(Chamada $chamada, int $user_id) {
        try {
            $user = User::findOrFail($user_id);
            $chamada->autor()->associate($user->id);
            $chamada->save();
        } catch (ModelNotFoundException $exception) {
            return response()->json('User not found', GeneralApiInterface::HTTP_401);
        }
    }
}
