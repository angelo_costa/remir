<?php

namespace App\Http\Controllers\Web\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Home;
use App\Noticia;
use App\Menu;
use App\Livro;
use App\Categoria;
use Log;

class HomeController extends Controller
{

    public function show() {
      $menu = Menu::orderBy('ordem', 'ASC')->get();

      $home = Home::latest()->first();
      $json = json_decode($home->template);
      $noticias = [];
      $categorias_destaque = [];

      foreach ($json->noticias as $item) {
        Log::info($item->ordem);
        $noticias[$item->ordem] =  Noticia::with('categoria', 'tags', 'imagens')->find($item->noticia_id);
      }

      foreach ($json->categorias_destaque as $categoria_id) {
        $categorias_destaque[] =  Categoria::find($categoria_id);
      }

      $publicacoes = Livro::orderBy('created_at', 'DESC')->take(3)->get();

      return view('home', [
        'noticias' => $noticias,
        'categorias_destaque' => $categorias_destaque,
        'menu' => $menu,
        'publicacoes' => $publicacoes
      ]);
    }
}
