<?php

namespace App\Http\Controllers\Web\Equipe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Livro;
use App\Menu;
use App\Equipe;
use Log;

class EquipeController extends Controller
{

    public function all(Request $request) {
      $menu = Menu::orderBy('ordem', 'ASC')->get();
      // $equipe = Equipe::orderBy('nome', 'ASC')->get();
      $universidades = Equipe::select('universidade')->distinct()->get();

      $equipe = (new Equipe)->newQuery()->orderBy('nome', 'ASC');

      if ($request->has('nome')) {
        $nome = $request->input('nome');

        if (isset($nome)) {
          $equipe->where('nome', 'like', '%' . $nome . '%');
        }
      }

      if ($request->has('universidade')) {
        $universidade = $request->input('universidade');

        if (isset($universidade)) {
          $equipe->where('universidade', 'like', '%' . $universidade . '%');
        }
      }


      return view ('equipe.index', [
        'equipe' => $equipe->get(),
        'menu' => $menu,
        'universidades' => $universidades,
      ]);
    }

    public function dados() {
      return Equipe::with('imagem')->get();
    }

}
