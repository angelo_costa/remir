<?php

namespace App\Http\Controllers\Web\Dado;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Dado;
use App\Menu;
use App\Tag;

class DadoController extends Controller
{

    public function show(string $slug) {
      $permalink = env('APP_URL') . '/biblioteca/' . $slug;

      $menu = Menu::orderBy('ordem', 'ASC')->get();

      $Dado = Dado::with('tags')->where('slug', $slug)->first();

      $Dado['leia_mais'] = $Dado->relacionados(2);

      return view('Dado.show', [
        'Dado' => $Dado,
        'menu' => $menu
      ]);
    }

    public function all(Request $request) {
      $dados = (new Dado)->newQuery();
      $dados->orderBy('created_at', 'DESC')->with('tags');
      $tags = Tag::all();
      $menu = Menu::orderBy('ordem', 'ASC')->get();

      if ($request->get('titulo')) {
        $dados->where('titulo', 'like', '%' . $request->get('titulo') . '%');
      }

      if ($request->get('tag')) {
        $dados->whereHas('tags', function($query) use ($request) {
          $query->where('tag_id', $request->get('tag'));
        })->get();

        // $dados->where('', 'like', '%' . $request->get('autor') . '%');
      }

      return view ('dado.index', [
        'dados' => $dados->paginate(20),
        'tags' => $tags,
        'menu' => $menu
      ]);
    }

}
