<?php

namespace App\Http\Controllers\Web\Remir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Remir;
use App\Http\Controllers\Api\ApiInterface\GeneralApiInterface;
use Validator;
use Input;
use App\Menu;

class RemirController extends Controller implements GeneralApiInterface
{



    /**
     * Recupera apenas uma categoria.
     *
     * @param int $categoriaId
     *      Id da categoria.
     *
     * @return App\Http\Resources\Categoria\CategoriaResource
     *      Retorna apenas o resource de uma categoria.
     */
    public function show() {
      $menu = Menu::orderBy('ordem', 'ASC')->get();
      $pagina = Remir::latest()->first();

      return view ('remir.show', [
        'pagina' => $pagina,
        'menu' => $menu
      ]);
    }
}
