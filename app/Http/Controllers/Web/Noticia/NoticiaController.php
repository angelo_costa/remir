<?php

namespace App\Http\Controllers\Web\Noticia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Imgur\ImgurService;
use App\Noticia;
use App\Menu;
use Validator;
use Log;

class NoticiaController extends Controller
{
    const PAGINATE = 20;

    /**
     * Recupera todas as notícias.
     *
     * @return App\Noticia
     *      Retorna a coleção de noticia.
     */
    public function all() {
        return Noticia::with('status', 'categoria')->paginate(self::PAGINATE);
    }

    /**
     * Recupera a noticia pelo permalink.
     * Exibição de uma notícia.
     *
     * @param string $permalink
     *
     */
    public function show(Request $request, string $slug) {
        $menu = Menu::orderBy('ordem', 'ASC')->get();

        $noticia = Noticia::with('tags')
          ->where('slug', $slug)->first();

        return view('noticia.show', [
          'noticia' => $noticia,
          'menu' => $menu
        ]);
    }
}
