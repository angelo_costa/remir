<?php

namespace App\Http\Controllers\Web\Noticia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NoticiaStatus;

class NoticiaStatusController extends Controller
{
    /**
     * Recupera todos os status.
     *
     * @return App\Http\Resources\Noticia\NoticiaStatusResourceCollection
     *      Retorna a coleção de status da noticia.
     */
    public function all() {
        return NoticiaStatus::all();
    }
}
