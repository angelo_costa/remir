<?php

namespace App\Http\Controllers\Web\Categoria;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categoria;
use App\Noticia;
use App\Menu;

class CategoriaController extends Controller
{
    /**
     * Recupera a categoria pelo permalink.
     * Retorna todas as noticias daquela categoria.
     *
     * @param string $permalink
     *
     */
    public function show(string $slug) {
      $menu = Menu::orderBy('ordem', 'ASC')->get();

      $categoria = Categoria::with('destaques.imagens', 'tags')
        ->where('slug', $slug)
        ->first();

      if (!$categoria) {
        return view('errors.404', [ 'menu' => $menu]);
      }

      $destaque_ids = $categoria->destaques->map(function($n) {
        return $n->id;
      });

      $noticias = Noticia::with('categoria', 'tags', 'imagens')
        ->where('categoria_id', $categoria->id)
        ->where('noticia_status_id', 2)
        ->whereNotIn('id', $destaque_ids)
        ->get();

      return view('categoria.show', [
        'noticias' => $noticias,
        'categoria' => $categoria,
        'menu' => $menu
      ]);
  }

}
