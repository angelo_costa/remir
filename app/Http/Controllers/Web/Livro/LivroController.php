<?php

namespace App\Http\Controllers\Web\Livro;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Livro;
use App\Menu;

class LivroController extends Controller
{

    public function show(string $slug) {
      $permalink = env('APP_URL') . '/biblioteca/' . $slug;

      $menu = Menu::orderBy('ordem', 'ASC')->get();

      $livro = Livro::with('tags')->where('slug', $slug)->first();

      $livro['leia_mais'] = $livro->relacionados(2);

      return view('livro.show', [
        'livro' => $livro,
        'menu' => $menu
      ]);
    }

    public function all(Request $request) {
      $anos = Livro::select('ano')->distinct()->get();
      $tipos = Livro::select('tipo')->distinct()->get();

      $livros = (new Livro)->newQuery();
      $livros->orderBy('created_at', 'DESC');

      if ($request->get('titulo')) {
        $livros->where('titulo', 'like', '%' . $request->get('titulo') . '%');
      }

      if ($request->get('autor')) {
        $livros->where('autor', 'like', '%' . $request->get('autor') . '%');
      }

      if ($request->get('ano')) {
        $livros->where('ano', $request->get('ano'));
      }

      if ($request->get('tipo')) {
        $livros->where('tipo', $request->get('tipo'));
      }

      $menu = Menu::orderBy('ordem', 'ASC')->get();

      return view ('livro.index', [
        'livros' => $livros->paginate(20),
        'anos' => $anos,
        'tipos' => $tipos,
        'menu' => $menu
      ]);
    }

}
