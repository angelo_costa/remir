<?php

namespace App\Http\Controllers\Web\Secao;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Secao;
use Validator;

class SecaoController extends Controller
{
    const PAGINATE = 20;

    /**
     * Recupera todas as seções.
     *
     * @return App\Secao
     *      Retorna a coleção de seções.
     */
    public function all() {
        return Secao::with('chamadas')->paginate(self::PAGINATE);
    }

    /**
     * Recupera apenas uma noticia.
     *
     * @param int $secaoId
     *      Id da seção.
     *
     * @return App\Secao
     *      Retorna apenas o resource de uma secao.
     */
    public function find(int $secaoId) {
        return Secao::with('chamadas')->find($secaoId);
    }

    /**
     * Cria uma seção.
     *
     * @param string $nivel
     *      Nivel de destaque da seção.
     * @param string $autorId
     *      Autor do autor da chamada.
     *
     * @return App\Secao
     *      Retorna um resource da secao.
     */
    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
            'nivel' => 'required|integer',
            'autorId' => 'string|nullable',
        ]);
        
        if ($validation->fails()) {
            return $validation->errors();
        }

        $secao = Secao::create([
            'nivel' => $request->nivel,
            'user_id' => $request->autorId
        ]);

        return $secao;
    }

    /**
     * Atualiza uma seção.
     *
     * @param string $nivel
     *      Nivel de destaque da seção.
     * @param string $autorId
     *      Autor do autor da chamada.
     *
     * @return App\Secao
     *      Retorna um resource da secao.
     */
    public function update(Request $request, int $secaoId) {
        $validation = Validator::make($request->all(),[
            'nivel' => 'required|integer',
            'autorId' => 'string|nullable',
        ]);

        if ($validation->fails()) {
            return $validation->errors();
        }

        $secao = Secao::findOrFail($secaoId);

        $secao->update($request->all());
        return $secao;
    }

    /**
     * Deleta uma seção.
     *
     * @param int $secaoId
     *      Id da seção.
     */
    public function delete(int $secaoId) {
        $secao = Secao::findaOrFail($secaoId);

        $secao->delete();
    }
}
