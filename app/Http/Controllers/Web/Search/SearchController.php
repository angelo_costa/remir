<?php

namespace App\Http\Controllers\Web\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Noticia;
use App\Categoria;
use App\Tag;
use App\Menu;
use App\Livro;

class SearchController extends Controller
{
  public function search(Request $request) {

    if ($request->has('q') && $request->input('q') != "") {
      $q = strtolower($request->input('q'));

      if ($request->has('como') && $request->input('como') != "") {
        $noticias = Noticia::where('autor', 'LIKE', '%'.$q.'%')->orderBy('created_at', 'DESC')->get();
        // $field = $request->input('como');
        // $noticias = Noticia::whereRaw($field.' LIKE "%'. $q .'%"')->orderBy('created_at', 'DESC')->get();
      } else {
        $noticias = Noticia::where(function ($query) use ($q) {
         $query
            ->orWhereRaw('LOWER(titulo) LIKE ?', '%'.$q.'%')
            ->orWhereRaw('LOWER(subtitulo) LIKE ?', '%'.$q.'%');
         })->orderBy('created_at', 'DESC')->get();
      }


      $categorias = Categoria::whereRaw('LOWER(nome) LIKE ?', '%'.$q.'%')->orderBy('created_at', 'DESC')->get();

      $tags = Tag::whereRaw('LOWER(nome) LIKE ?', '%'.$q.'%')->orderBy('created_at', 'DESC')->get();

      $livros = Livro::whereRaw('LOWER(titulo) LIKE ? ', '%'.$q.'%')->orderBy('created_at', 'DESC')->get();

      return view('pesquisa.index', [
        'noticias' => $noticias,
        'categorias' => $categorias,
        'tags' => $tags,
        'livros' => $livros,
        'menu' => Menu::orderBy('ordem', 'ASC')->get(),
        'query' => $request->input('q')
      ]);
    } else {
      return back()->withInput();
    }
  }
}
