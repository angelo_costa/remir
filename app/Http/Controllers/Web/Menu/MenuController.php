<?php

namespace App\Http\Controllers\Web\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use Validator;
class MenuController extends Controller
{
    /**
     * Recupera todos as menus.
     *
     * @return App\Http\Resources\Menu\MenuResourceCollection
     *      Retorna a coleção de menu.
     */
    public function all() {
        return Menu::all();
    }

    /**
     * Recupera apenas uma categoria.
     *
     * @param int $menuId
     *      Id do menu.
     *
     * @return App\Http\Resources\Menu\MenuResource
     *      Retorna apenas o resource de um menu.
     */
    public function find(int $menuId) {
        return Menu::findOrFail($menuId);
    }


    /**
     * Cria um menu.
     *
     * @param string $nome
     *      Nome do menu
     *
     * @return App\Http\Resources\Menu\MenuResource]
     *      Retorna um resource menu.
     */
    public function create(Request $request) {
        $validation = Validator::make($request->all(), [
            'nome' => 'required|string|unique:menu',
        ]);

        if ($validation->fails()) {
            return $validation->errors();
        }

        $menu = Menu::create([
            'nome' => $request->nome,
        ]);

        return $menu;
    }

    /**
     * Atualiza um menu.
     *
     * @param string $nome
     *      Nome do menu.
     * @param int $menuId
     *      Id do menu.
     *
     * @return App\Http\Resources\Menu\MenuResource]
     *      Retorna um resource menu.
     */
    public function update(Request $request, int $menuId) {
        $validation = Validator::make($request->all(), [
            'nome' => 'required|string|unique:menu',
        ]);

        if ($validation->fails()) {
            return $validation->errors();
        }

        $menu = Menu::findOrFail($menuId);

        $menu->nome = $request->nome;
        $menu->save();

        return $menu;
    }


    /**
     * Atualiza um menu.
     *
     * @param int $menuId
     *      Id do menu.
     *
     * @return App\Http\Resources\Menu\MenuResource]
     *      Retorna um resource menu.
     */
    public function delete(int $menuId) {
        $menu = Menu::findOrFail($menuId);

        $menu->delete();
    }

}
