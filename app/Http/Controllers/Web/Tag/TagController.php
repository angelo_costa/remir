<?php

namespace App\Http\Controllers\Web\Tag;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;
use App\Noticia;
use App\Menu;

class TagController extends Controller
{
    /**
     * Recupera a categoria pelo permalink.
     * Retorna todas as noticias daquela categoria.
     *
     * @param string $permalink
     *
     */
    public function show(string $slug) {
      $menu = Menu::orderBy('ordem', 'ASC')->get();

      $tag = Tag::where('slug', $slug)->first();

      $noticias = $tag->noticias;

      return view('tags.show', [
        'tag' => $tag,
        'noticias' => $noticias,
        'menu' => $menu
      ]);
  }

}
