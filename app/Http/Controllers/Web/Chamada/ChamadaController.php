<?php

namespace App\Http\Controllers\Web\Chamada;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Chamada;
use App\Noticia;
use Validator;

class ChamadaController extends Controller
{
    const PAGINATE = 20;

    /**
     * Recupera todas as chamadas.
     *
     * @return App\Chamada
     *      Retorna a coleção de chamada.
     */
    public function all() {
        return Chamada::with('noticia')->paginate(self::PAGINATE);
    }

    /**
     * Recupera apenas uma chamada.
     *
     * @param int $chamadaId
     *      Id da chamada.
     *
     * @return App\Chamada
     *      Retorna apenas o resource de uma chamada.
     */
    public function find(int $chamadaId) {
        return Chamada::with('noticia')->findOrFail($chamadaId);
    }

    /**
     * Cria uma chamada.
     *
     * @param string $titulo
     *      Titulo da chamada.
     * @param int $noticiaId
     *      Id da noticia.
     *
     * @return App\Http\Resources\Noticia\NoticiaResource
     *      Retorna um resource da noticia.
     */
    public function create(Request $request) {
        $validation = Validator::make($request->all(),[
            'titulo' => 'required|string',
            'noticia_id' => 'integer|nullable',
        ]);

        if ($validation->fails()) {
            return $validation->errors();
        }
        $chamada = Chamada::create([
            'titulo' => $request->titulo,
            'noticia_id' => $request->noticia_id
        ]);

        return $chamada::with('noticia');
    }

    /**
     * Cria uma chamada.
     *
     * @param int $chamadaId
     *      Id da chamda.
     * @param string $titulo
     *      Titulo da chamada.
     * @param int $noticiaId
     *      Id da noticia.
     *
     * @return App\Http\Resources\Noticia\NoticiaResource
     *      Retorna um resource da noticia.
     */
    public function update(Request $request, int $chamadaId) {
        $validation = Validator::make($request->all(),[
            'titulo' => 'required|string',
            'noticia_id' => 'integer|nullable',
        ]);

        if ($validation->fails()) {
            return $validation->errors();
        }

        $chamada = Chamada::findOrFail($chamadaId);

        $chamada->update($request->all());

        return $chamada::with('noticia');
    }

    /**
     * Deleta uma chamada.
     *
     * @param int $chamadaId
     *      Id da chamada.
     */
    public function delete(int $chamadaId) {
        $chamada = Chamada::findaOrFail($chamadaId);

        $chamada->delete();
    }
}
