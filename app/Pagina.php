<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pagina extends Model
{
    use SoftDeletes;

    protected $table = 'paginas';

    protected $fillable = [
        'titulo',
        'slug',
        'menu_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function chamadas() {
        return $this->hasMany('App\Chamada');
    }

    public function menu() {
        return $this->belongsTo('App\Menu');
    }
}
