<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Livro extends Model
{
    use SoftDeletes;

    protected $table = 'livros';

    protected $fillable = [
        'titulo',
        'autor',
        'orientador',
        'ano',
        'tipo',
        'instituicao',
        'sigla',
        'repositorio',
        'resumo',
        'arquivo',
        'permalink',
        'slug',
        'editora'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function tags() {
      return $this->belongsToMany('App\Tag', 'livro_tag');
    }

    public function relacionados($quantidade) {
      $related = self::whereNotIn('id', [$this->id])
            ->orderBy('created_at', 'DESC')
            ->take($quantidade)
            ->get();

      return $related;
    }
}
