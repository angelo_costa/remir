<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipe extends Model
{
    use SoftDeletes;

    protected $table = 'equipe';

    protected $fillable = [
      'nome',
      'email',
      'universidade',
      'curriculo',
      'local',
      'lat',
      'lng',
      'imagem_id',
    ];

    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at'
    ];

    public function imagem() {
      return $this->belongsTo('App\Imagem', 'imagem_id', 'id');
    }
}
