<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Log;

class Noticia extends Model
{
    use SoftDeletes;

    protected $table = 'noticias';

    protected $fillable = [
      'titulo',
      'corpo',
      'subtitulo',
      'imagem',
      'local',
      'noticia_status_id',
      'categoria_id',
      'user_id',
      'imagem_id',
      'permalink',
      'slug',
      'autor',
      'video_destaque',
    ];

    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
      'published_at'
    ];

    public function categoria() {
      return $this->belongsTo('App\Categoria');
    }

    public function status() {
      return $this->belongsTo('App\NoticiaStatus', 'noticia_status_id', 'id');
    }

    public function autor() {
      return $this->hasMany('App\User');
    }

    public function imagens() {
      return $this->belongsToMany('App\Imagem', 'imagem_noticia', 'id');
    }

    public function tags() {
      return $this->belongsToMany('App\Tag', 'noticia_tag');
    }

    /**
     * Recupera a noticias de mesma categoria ordenando por data de criação.
     * E não repetindo o id da noticia que está sendo exibida.
     */
    public function relacionados($quantidade) {
      $related = self::with('categoria','status')
            ->whereNotIn('id', [$this->id])
            ->orderBy('created_at', 'DESC')
            ->where('categoria_id', '=', $this->categoria->id)
            ->take($quantidade)
            ->get();

      return $related;
    }

    public function imagem() {
      $img = $this->imagens()->first();
      return $img;
    }
}
