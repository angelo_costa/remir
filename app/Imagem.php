<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imagem extends Model {

    use SoftDeletes;

    protected $table = 'imagens';

    protected $fillable = [
        'titulo',
        'big_url',
        'small_url',
        'legenda',
        'creditos',
        'cloudnary',
    ];

    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at'
  ];

  public function getCloudnaryAttribute($value) {
    if (!is_null($value) && $value != '') {
      return json_decode($value);
    } else {
      return false;
    }
  }
}
