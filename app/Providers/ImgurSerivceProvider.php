<?php

namespace App\Providers;

use App\Services\Imgur\ImgurService;
use App\Services\Imgur\ImgurServiceInterface;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class ImgurSerivceProvider extends ServiceProvider
{
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ImgurServiceInterface::class,  function($app) {
            return new ImgurService(Client::class);
        });
    }
}
