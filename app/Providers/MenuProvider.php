<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Menu;

class MenuProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $menu = Menu::orderBy('ordem', 'ASC')->get();
            $view->with('menu', $menu);
        });
    }
}
