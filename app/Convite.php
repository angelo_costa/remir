<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Convite extends Model
{
    protected $table = 'convite';

    protected $fillable = [
        'email',
        'token',
    ];
}
