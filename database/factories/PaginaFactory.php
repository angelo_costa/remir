<?php

use Faker\Generator as Faker;

$factory->define(App\Pagina::class, function (Faker $faker) {
    return [
        'titulo' => $faker->titulo,
        'slug' => $faker->slug,
        'menu_id' => $faker->menu_id,
    ];
});
