<?php

use Faker\Generator as Faker;

$factory->define(App\Menu::class, function (Faker $faker) {
    return [
        'nome' => $faker->nome,
        'link' => $faker->link,
    ];
});
