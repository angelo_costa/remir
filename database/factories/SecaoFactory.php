<?php

use Faker\Generator as Faker;

$factory->define(App\Secao::class, function (Faker $faker) {
    return [
        'nivel' => $faker->nivel,
        'chamada_id' => $faker->chamada_id,
    ];
});
