<?php

use Faker\Generator as Faker;

$factory->define(App\Categoria::class, function (Faker $faker) {
    return [
        'nome' => $faker->text(45),
        'descricao' => $faker->text,
    ];
});
