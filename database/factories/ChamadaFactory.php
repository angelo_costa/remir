<?php

use Faker\Generator as Faker;

$factory->define(App\Chamada::class, function (Faker $faker) {
    return [
        'titulo' => $faker->text(45),
        'corpo' => $faker->text,
        'noticia_id' => factory(App\Noticia::class),
        'user_id' => factory(App\User::class),
    ];
});
