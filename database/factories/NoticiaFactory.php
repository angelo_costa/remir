<?php

use Faker\Generator as Faker;

$factory->define(App\Noticia::class, function (Faker $faker) {
    return [
        'titulo' => $faker->text,
        'corpo' => $faker->text,
        'subtitulo' => $faker->text,
        'local' => $faker->text,
        //'noticia_status_id' => $faker->noticia_status_id,
        //'categoria_id' => $faker->categoria_id,
        //'user_id' => $faker->user_id,
        //'imagem_id' => $faker->imagem_id,
    ];
});
