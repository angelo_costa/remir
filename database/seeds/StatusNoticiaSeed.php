<?php

use Illuminate\Database\Seeder;

class StatusNoticiaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('noticia_status')->insert([
            'nome' => "Rascunho",
        ]);

        DB::table('noticia_status')->insert([
            'nome' => "Publicado",
        ]);
    }
}
