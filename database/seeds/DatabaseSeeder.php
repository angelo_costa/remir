<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      DB::table('noticia_status')->insert([
        'nome' => "Rascunho",
      ]);

      DB::table('noticia_status')->insert([
          'nome' => "Publicado",
      ]);
    }
}
