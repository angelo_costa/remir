<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Secao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secoes', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->text('titulo')->nullable()->comment('Titulo da seção');
          $table->text('id_interno')->nullable()->comment('ID interno da seção');
          $table->enum('orientacao', ['horizontal', 'vertical'])->nullable()->commemt('Orientação da seção'); 
          $table->softDeletes();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('secoes');
    }
}
