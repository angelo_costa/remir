<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlteraColunaRepositorioArquivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('livros', function($table) {
            $table->longText('repositorio')->change()->nullable();
            $table->longText('arquivo')->change()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('livros', function($table) {
            $table->dropColumn('repositorio');
            $table->dropColumn('arquivo');
       });
    }
}
