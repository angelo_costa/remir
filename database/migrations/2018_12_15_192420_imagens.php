<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Imagens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagens', function(Blueprint $table) {
            $table->bigIncrements('id')->comments('Id da imagem');
            $table->string('titulo')->nullable()->comment('Titulo');
            $table->string('url')->nullable()->comment('URL da imagem');
            $table->string('tamanho');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagens');
    }
}
