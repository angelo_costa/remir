<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLivros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('livros', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->string('titulo');
          $table->string('autor');
          $table->string('orientador')->nullable();
          $table->integer('ano')->nullable();
          $table->string('tipo')->nullable();
          $table->string('instituicao')->nullable()->nullable();
          $table->string('repositorio')->nullable();
          $table->longText('resumo')->nullable();
          $table->string('arquivo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('livros');
    }
}
