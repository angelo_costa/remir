<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SecaoChamada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chamada_secao', function(Blueprint $table) {
            $table->bigIncrements('id')->comments('Id da imagem');
            $table->bigInteger('chamada_id')->unsigned()->nullable()->comment('Id da chamada');
            $table->bigInteger('secao_id')->unsigned()->nullable()->comment('Id da seção');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chamada_secao');
    }
}
