<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChamadaImagens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagem_chamada', function(Blueprint $table) {
            $table->bigIncrements('id')->comments('Id da imagem');
            $table->bigInteger('chamada_id')->unsigned()->nullable()->comment('Id da chamada');
            $table->bigInteger('imagem_id')->unsigned()->nullable()->comment('Id da imagem');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagem_chamada');
    }
}
