<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Menu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
          $table->increments('id');
          $table->string('titulo')->comment('Nome do menu');;
          $table->string('link_externo')->nullable()->comment('Link para a pagina');
          $table->unsignedInteger('pagina_id')->nullable()->unsiged();
          $table->foreign('pagina_id')->references('id')->on('paginas');
          $table->softDeletes();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
