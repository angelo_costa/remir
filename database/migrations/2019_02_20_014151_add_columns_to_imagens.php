<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToImagens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('imagens', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('tamanho');

            $table->string('small_url')->nullable();
            $table->string('big_url')->nullable();
            $table->string('legenda')->nullable();
            $table->string('creditos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('imagens', function (Blueprint $table) {
            $table->dropColumn('legenda');
            $table->dropColumn('creditos');
            $table->dropColumn('small_url');
            $table->dropColumn('big_url');

            $table->string('tamanho');
            $table->string('url');
        });
    }
}
