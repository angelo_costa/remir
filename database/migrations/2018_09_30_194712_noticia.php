<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Noticia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('titulo')->comment('Titulo da noticia');
            $table->string('subtitulo')->nullable()->comment('Subtitulo da noticia');
            $table->longText('corpo')->nullable()->comment('Corpo da noticia');
            $table->string('local')->nullable()->comment('Local da noticia');
            $table->unsignedInteger('categoria_id')->nullable()->unsiged();
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->unsignedInteger('noticia_status_id')->nullable()->unsiged();
            $table->foreign('noticia_status_id')->references('id')->on('noticia_status');
            $table->unsignedInteger('user_id')->nullable()->unsiged();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamp('published_at')->nullable()->comment('Data de publicação');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticias');
    }
}
