<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NoticiaImagens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagem_noticia', function(Blueprint $table) {
            $table->bigIncrements('id')->comments('Id da imagem');
            $table->bigInteger('noticia_id')->unsigned()->nullable()->comment('Id da noticia');
            $table->bigInteger('imagem_id')->unsigned()->nullable()->comment('Id da imagem');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagem_noticia');
    }
}
