@extends('layouts.base', [ "menu" => $menu])

@section('css')
<style>
  @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
</style>
@endsection

@section('js')
@endsection

@section('content')
<div class="container livros-page">

  <article class="main-category-section my-5">
    <h1 class="category-title">
      <a href="/dados">Dados</a>
    </h1>
  </article>

  <div class="row mb-5">
    <section class="col-md-8">
      @if (count($dados) < 1)
        <h2 class="empty-result">Nenhum resultado encontrado.</h2>
      @endif

      @foreach($dados as $dado)
      <div class="row mb-5">
        <div class="col-md-12">
          <article class="book">
            <div class="meta top mb-2">
            @foreach($dado->tags as $tag)
              <span class="tipo mr-2">
                  {{ $tag->nome }}
              </span>
              @endforeach
            </div>
            <h2 class="title">
              <a href="{{ $dado->link }}" target="_blank">
                {{ $dado->titulo }}
              </a>
            </h2>
          </article>
        </div>
      </div>
      @endforeach
      <div class="mb-5">
        {{ $dados->links() }}
      </div>
    </section>

    <section class="col-md-4">
      <div class="bordered-section">
        <h3 class="section-title-wrapper">
          <span class="section-title"> Pesquisar </span>
        </h3>
        <form id="books-filter" class="filter" method="GET">
          <!-- <label class="title">Filtros</label> -->
          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Título</label>
                <input name="titulo" type="text" class="form-control" value="{{ request()->input('titulo') }}" placeholder="Pesquisa por título">
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Tags</label>
                <select name="tag" class="select form-control">
                  <option value="" selected>Todas</option>
                    @foreach($tags as $tag)
                      @if (request()->input('tag') == $tag->id)
                        <option value="{{ $tag->id}}" selected> {{ $tag->nome }} </option>
                      @else
                        <option value="{{ $tag->id}}"> {{ $tag->nome }} </option>
                      @endif
                    @endforeach
                </select>
              </div>
            </div>
          </div>



          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group mb-0">
                <input class="btn btn-block submit" type="submit" value="Filtrar" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>

</div>
@endsection
