@extends('layouts.base', [ "menu" => $menu])
@section('meta')
<title>REMIR Trabalho</title>
@endsection
@section('css')
<style>
  @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
</style>
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/tags-nuvem.js') }}"></script>
@endsection

@section('content')
<div class="container mt-5 category-page">

  <!-- @if(count($categorias_destaque) > 0)
  <div class="row">
    <div class="col-md-12">
      <section class="tags mb-5">
        <h3 class="label mb-4">CATEGORIAS</h3>
        <ul class="tags-list list list-unstyled">

          @foreach($categorias_destaque as $categoria)
            <li class="tag">
              <a href="{{ $categoria->permalink }}"> {{ $categoria->nome }} </a>
            </li>
          @endforeach
        </ul>
      </section>
    </div>
  </div>
  @endif -->

  <!-- HIGHLIGHTS -->
  <div class="row highlights">
    <div class="col-md-6 story mb-5 mb-md-5 mb-lg-0 mb-xl-0">
      @include('partials.noticia', [ 'noticia' => $noticias[1]])
    </div>

    <div class="col-md-6 story mb-5 mb-md-5 mb-lg-0 mb-xl-0">
      @include('partials.noticia', [ 'noticia' => $noticias[2]])
    </div>
  </div>

  <!-- END HIGHLIGHTS -->

  <div class="row my-md-5">
    <div class="col-md-9">
      <div class="row mb-5">
        <div class="col-md-4 col-lg-4 story mb-5 mb-md-5 mb-lg-0 mb-xl-0">
          @include('partials.noticia', [ 'noticia' => $noticias[3]])
        </div>

        <div class="col-md-4 col-lg-4 mb-5 mb-md-5 mb-lg-0 mb-xl-0">
          @include('partials.noticia', [ 'noticia' => $noticias[4]])
        </div>

        <div class="col-md-4 col-lg-4 mb-5 mb-md-5 mb-lg-0 mb-xl-0">
          @include('partials.noticia', [ 'noticia' => $noticias[5]])
        </div>
      </div>

      <div class="row">
        <div class="col-md-4 col-lg-4 story mb-5 mb-md-5 mb-lg-0 mb-xl-0">
          @include('partials.noticia', [ 'noticia' => $noticias[6]])
        </div>

        <div class="col-md-4 col-lg-4 mb-5 mb-md-5 mb-lg-0 mb-xl-0">
          @include('partials.noticia', [ 'noticia' => $noticias[7]])
        </div>

        <div class="col-md-4 col-lg-4 mb-5 mb-md-5 mb-lg-0 mb-xl-0">
          @include('partials.noticia', [ 'noticia' => $noticias[8]])
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <section class="tag-cloud-container">
        <h3 class="title mb-4">Tags</h3>
        <div class="tags-cloud"></div>
      </section>
    </div>
  </div>

  <div class="row my-md-5">
    <div class="col-md-3">
      <div class="row mb-5">
        <div class="col-md-12">
          @include('partials.noticia', [ 'noticia' => $noticias[9]])
        </div>
      </div>
      <div class="row mb-5">
        <div class="col-md-12">
          @include('partials.noticia', [ 'noticia' => $noticias[10]])
        </div>
      </div>
    </div>
    <div class="col-md-6 mb-5">
      <section class="books bordered-section">
        <h3 class="section-title-wrapper">
          <span class="section-title"> Últimas Publicações </span>
        </h3>
        @foreach($publicacoes as $publicacao)
        <div class="row @if ($loop->last === false) mb-5 @endif">
          <div class="col-md-12">
            <article class="book">
              <div class="meta top mb-2">
                <span class="tipo">
                  {{ $publicacao->tipo }}
                </span>
              </div>
              <h2 class="title">
                <a href="{{ $publicacao->permalink }}">
                  {{ $publicacao->titulo }}
                </a>
              </h2>
              <div class="meta bottom">
                <span class="author"> {{ $publicacao->autor }} </span>
                <span class="year"> {{ $publicacao->ano }} </span>
                <span class="institution"> {{ $publicacao->sigla }} </span>
              </div>
            </article>
          </div>
        </div>
        @endforeach
      </section>
    </div>
    <div class="col-md-3">
      <div class="row mb-5">
        <div class="col-md-12">
          @include('partials.noticia', [ 'noticia' => $noticias[11]])
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          @include('partials.noticia', [ 'noticia' => $noticias[12]])
        </div>
      </div>
    </div>
  </div>

  <div class="row mb-5">
    <div class="col-md-3 col-lg-3 story mb-5 mb-md-5 mb-lg-0 mb-xl-0">
      @include('partials.noticia', [ 'noticia' => $noticias[13]])
    </div>

    <div class="col-md-3 col-lg-3 mb-5 mb-md-5 mb-lg-0 mb-xl-0">
      @include('partials.noticia', [ 'noticia' => $noticias[14]])
    </div>

    <div class="col-md-3 col-lg-3 mb-5 mb-md-5 mb-lg-0 mb-xl-0">
      @include('partials.noticia', [ 'noticia' => $noticias[15]])
    </div>

    <div class="col-md-3 col-lg-3 mb-5 mb-md-5 mb-lg-0 mb-xl-0">
      @include('partials.noticia', [ 'noticia' => $noticias[16]])
    </div>
  </div>
</div>
@endsection
