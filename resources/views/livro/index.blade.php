@extends('layouts.base', [ "menu" => $menu])
@section('meta')
<title>Biblioteca | REMIR Trabalho</title>
@endsection
@section('css')
<style>
  @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
</style>
@endsection

@section('js')
@endsection

@section('content')
<div class="container livros-page">

  <article class="main-category-section my-5">
    <h1 class="category-title">
      <a href="/biblioteca">Biblioteca</a>
    </h1>
  </article>

  <div class="row mb-5">
    <section class="col-md-8">
      @if (count($livros) < 1)
        <h2 class="empty-result">Nenhum resultado encontrado.</h2>
      @endif

      @foreach($livros as $livro)
      <div class="row mb-5">
        <div class="col-md-12">
          <article class="book">
            <div class="meta top mb-2">
              <span class="tipo">
                {{ $livro->tipo }} 
              </span>
            </div>
            <h2 class="title">
              <a href="{{ $livro->permalink }}">
                {{ $livro->titulo }}
              </a>
            </h2>
            <div class="meta bottom">
              <span class="author"> {{ $livro->autor }} </span>
              <span class="year"> {{ $livro->ano }} </span>
              <span class="institution"> {{ $livro->sigla }} </span>
            </div>
          </article>
        </div>
      </div>
      @endforeach
      <div class="mb-5">
        {{ $livros->links() }}
      </div>
    </section>

    <section class="col-md-4">
      <div class="bordered-section">
        <h3 class="section-title-wrapper">
          <span class="section-title"> Pesquisar </span>
        </h3>
        <form id="books-filter" class="filter" method="GET">
          <!-- <label class="title">Filtros</label> -->
          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Título</label>
                <input name="titulo" type="text" class="form-control" value="{{ request()->input('titulo') }}" placeholder="Pesquisa por título">
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Autor(a)</label>
                <input name="autor" type="text" class="form-control" value="{{ request()->input('autor') }}" placeholder="Pesquisa por autor">
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Ano</label>
                <select name="ano" class="select form-control" >
                  <option value="" selected>Todos</option>
                    @foreach($anos as $ano)
                      @if (request()->input('ano') == $ano->ano)
                        <option value="{{ $ano->ano}}" selected> {{ $ano->ano }} </option>
                      @else
                        <option value="{{ $ano->ano}}"> {{ $ano->ano }} </option>
                      @endif
                    @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Tipo</label>
                <select name="tipo" class="select form-control">
                  <option value="" selected>Todos</option>
                    @foreach($tipos as $tipo)
                      @if (request()->input('tipo') == $tipo->tipo)
                        <option value="{{ $tipo->tipo}}" selected> {{ $tipo->tipo }} </option>
                      @else
                        <option value="{{ $tipo->tipo}}"> {{ $tipo->tipo }} </option>
                      @endif
                    @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group mb-0">
                <input class="btn btn-block submit" type="submit" value="Filtrar" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>

</div>
@endsection
