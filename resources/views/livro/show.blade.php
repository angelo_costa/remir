@extends('layouts.base', [ "menu" => $menu ])

@section('css')
<style>
  @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
</style>
<link type="text/css" rel="stylesheet" href="{{ URL::asset('css/jssocials.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ URL::asset('css/jssocials-theme-plain.css') }}" />
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/jssocials.min.js') }}"" ></script>
<script type="text/javascript" src="{{ URL::asset('js/book-page-share-buttons.js') }}"></script>
@endsection

@section('content')

<div class="container small">
<div class="jssocial desktop side rounded news-show" id="desktop-share-buttons"></div>
  <article class="news news-show book">
    <h3 class="mt-5 mb-1 category">
      <a href="/biblioteca?tipo={{ $livro->tipo }}">
        {{ $livro->tipo }}
      </a>
    </h3>
    <h1 class="my-4 title">
      {{$livro->titulo}}
    </h1>

    <div class="meta mb-5">
      <span class="published-at mr-2">
        {{ \Carbon\Carbon::parse($livro->created_at)->formatLocalized('%d de %B de %Y') }}
      </span>
      <span class="author">
        <span class="repositorio mr-2"> <a target="_blank" href="{{ $livro->repositorio }} "> Repositório </a> </span>
        <span class="arquivo"> <a target="_blank" href="{{ $livro->arquivo }}">Download</a>  </span>
      </span>
    </div>

    <section id="news-body" class="body">

      <p>
        <div>
          <strong>Autor(a)</strong>
        </div>
        <span>{{ $livro->autor }}</span>
      </p>

      <p>
        <div>
          <strong>Orientador(a)</strong>
        </div>
        <span>{{ $livro->orientador }}</span>
      </p>

      <p>
        <div>
          <strong>Ano de publicação</strong>
        </div>
        <span>{{ $livro->ano }}</span>
      </p>

      @if(!is_null($livro->editora))
      <p>
        <div>
          <strong>Editora</strong>
        </div>
        <span>{{ $livro->editora }}</span>
      </p>
      @endif

      <p>
        <div>
          <strong>Tipo</strong>
        </div>
        <span>{{ $livro->tipo }}</span>
      </p>


      <p>
        <div>
          <strong>Instituição</strong>
        </div>
        <span>{{ $livro->instituicao }} - {{ $livro->sigla}} </span>
      </p>

      <p>
        <div>
          <strong>Resumo</strong>
        </div>
        <span>{{ $livro->resumo }}<span>
      </p>

      {{ $livro->resumo }}
    </section>
  </article>

  @if (count($livro->tags) > 0)
    <section class="tags my-5">
      <h3 class="label mb-4">TAGS</h3>
      <ul class="tags-list list list-unstyled">

        @foreach($livro->tags as $tag)
          <li class="tag">
            <a href="{{ $tag->permalink }}"> {{ $tag->nome }} </a>
          </li>
        @endforeach
      </ul>
    </section>
  @endif

  @if(count($livro->leia_mais) > 0)
  <section class="more my-5">
    <h3 class="label mb-4">LEIA MAIS</h3>
      <div class="row">
        @foreach($livro->leia_mais as $livro_relacionada)
          <article class="col-sm story mb-5 mb-md-0 mb-lg-0 mb-xl-0">
            <h2 class="title">
              <a  href="{{ $livro_relacionada->permalink }}"> {{ $livro_relacionada->titulo }} </a>
            </h2>

            <div class="meta">
              <span class="type"> {{ $livro->tipo }} </span>
              por
              <a href="/biblioteca?autor={{$livro->autor}}" class="author"> {{ $livro->autor }} </span>
            </div>
          </article>
        @endforeach
      </div>
    </ul>
  </section>
  @endif

</div>


@endsection
