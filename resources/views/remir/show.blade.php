@extends('layouts.base', [ "menu" => $menu])
@section('meta')
<title>A REMIR | REMIR Trabalho</title>
@endsection
@section('css')
<style>
  @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
  @import url('https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.5.1/leaflet.css');
  @import url('https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css');
  @import url('https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css');
</style>
@endsection

@section('js')
@endsection

@section('content')

<div class="container medium my-5">
<article class="news news-show">
    <h1 class="my-4 title">
      {{$pagina->titulo}}
    </h1>

    <section id="news-body" class="body">
      {!! str_replace('<p><br></p>', '', $pagina->corpo) !!}
      <div class="jssocial mobile inline rounded news-show" id="mobile-share-buttons"></div>
    </section>
  </article>
</div>
@endsection
