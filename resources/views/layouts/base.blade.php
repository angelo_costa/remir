<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- <title>REMIR | Reforma Trabalhista</title> -->
  @yield('meta')

  @yield('head')
  <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
  @yield('css')

</head>
<body>

  @include('partials.topbar')
  @include('partials.menu', [ "menu" => $menu ])

  <div class="content">
    @yield('content')
  </div>

  @include('partials.footer', [ "menu" => $menu ])

  <script src="{{ asset('js/app.js') }}"></script>
  @yield('js')

</body>
</html>
