@extends('layouts.base', [ "menu" => $menu])
@section('meta')
<title>Equipe | REMIR Trabalho</title>
@endsection
@section('css')
<style>
  @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
  @import url('https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.5.1/leaflet.css');
  @import url('https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css');
  @import url('https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css');
</style>
@endsection

@section('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.5.1/leaflet-src.js"></script>
<script type="text/javascript" src="https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/equipe.js') }}"></script>
@endsection

@section('content')
<div>
  <div id="map" class="map"></div>
</div>

<div class="container medium my-5">
  <div class="row">
    <section class="col-md-12">
      <div class="bordered-section">
        <h3 class="section-title-wrapper">
          <span class="section-title"> Pesquisar </span>
        </h3>
        <form id="books-filter" class="filter" method="GET">
          <!-- <label class="title">Filtros</label> -->
          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nome</label>
                <input name="nome" type="text" class="form-control" value="{{ request()->input('nome') }}" placeholder="Pesquisa por nome">
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Universidade</label>
                <select name="universidade" class="select form-control" >
                  <option value="" selected>Todas</option>
                    @foreach($universidades as $universidade)
                      @if (request()->input('universidade') == $universidade->universidade)
                        <option value="{{ $universidade->universidade}}" selected> {{ $universidade->universidade }} </option>
                      @else
                        <option value="{{ $universidade->universidade}}"> {{ $universidade->universidade }} </option>
                      @endif
                    @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group mb-0">
                <input class="btn btn-block submit" type="submit" value="Pesquisar" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>
</div>
<div class="container medium my-5">
  <div class="row">
  @foreach($equipe as $membro)
    <div class="col-md-6 mb-5">
      <article class="member">
        @if($membro->imagem)
        <div class="mr-3">
          <div class="avatar" style="background-image: url('{{ $membro->imagem->cloudnary->secure_url }}')"></div>
        </div>
        @endif
        <div>
          <h2 class="name">{{ $membro->nome }}</h2>
          <div>
            <div class="university">{{ $membro->local }}</div>
            <div class="meta">
              <span>{{ $membro->universidade }}</span>
              <span><a href="mailto:{{ $membro->email}}"> E-mail </a></span>
              <span><a target="_blank" href="{{ $membro->curriculo}}"> Lattes </a></span>
            </div>
          </div>
        </div>
      </article>
    </div>
  @endforeach
  </div>
</div>
@endsection
