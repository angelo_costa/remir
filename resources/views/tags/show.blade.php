@extends('layouts.base', [ "menu" => $menu])

@section('meta')
<title>{{ $tag->nome }} | REMIR Trabalho</title>
@endsection

@section('css')
<style>
  @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
</style>
<link type="text/css" rel="stylesheet" href="{{ URL::asset('css/jssocials.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ URL::asset('css/jssocials-theme-plain.css') }}" />
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/jssocials.min.js') }}"" ></script>
<script type="text/javascript" src="{{ URL::asset('js/news-page-share-buttons.js') }}"></script>
@endsection

@section('content')
<div class="container category-page">

  <article class="main-category-section my-5">
    <h1 class="category-title">
      <a href="{{ $tag->permalink }}">{{ $tag->nome }}</a>
    </h1>
  </article>

  @foreach($noticias->chunk(4) as $chunk)
    <div class="row my-md-5">
      @foreach($chunk as $noticia)
      <div class="col-md-4 col-lg-3 story mb-5 mb-md-5 mb-lg-0 mb-xl-0">
        @if(!empty($noticia->imagem()))
        <a href="{{$noticia->permalink}}">
          <img class="img" src="{{ $noticia->imagem()->cloudnary->secure_url }}" />
        </a>
        @endif
        <div class="meta">
          <span class="category"> {{ $noticia->categoria->nome}} </span>
            | por:
          <span class="author"> <a href="#"> REMIR</a> </span>
        </div>

        <h2 class="title">
          <a href="{{$noticia->permalink}}"> {{ $noticia->titulo }} </a>
        </h2>
      </div>
      @endforeach
    </div>
  @endforeach
</div>
@endsection
