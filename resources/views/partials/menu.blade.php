<section class="container">
  <nav class="menu mt-2 mb-5 d-none d-lg-flex desktop">
    @foreach($menu as $item)
      <a href="{{ $item->permalink }}"> {{ $item->titulo }} </a>
    @endforeach
  </nav>
</section>

<nav id="menu-mobile" class="menu mb-5 d-lg-none mobile">
  @foreach($menu as $item)
    <a href="{{ $item->permalink }}"> {{ $item->titulo }} </a>
  @endforeach
</nav>