<header class="topbar">
  <div class="container d-flex">
    <div class="left">
      <a href="#" class="menu-trigger" data-toggle="#menu-mobile">
        <i class="fa fa-bars"></i>
      </a>

      <a href="/a-remir">A REMIR</a>
      <!-- <a href="#">Trabalhos</a> -->
    </div>
    <div class="center">
      <a href="/"><img class="logo" src=" {{ asset('images/remir.png') }}" /></a>
    </div>
    <div class="right">
      <form id="search-form" class="search-form" method="get" action="/pesquisa">
        <input id="q" name="q" type="text" class="query" value="" size="10" tabindex="1">
        <input type="submit" class="button submit" value="" tabindex="2">
      </form>
      <a href="https://fb.me/RemirTrabalho" target="_blank" class="social-link">
        <!-- <img src=" {{ asset('images/facebook.png')}} " /> -->
        <i class="fa fa-facebook"></i>
      </a>
      <a href="https://twitter.com/RemirTrabalho" target="_blank" class="social-link">
        <!-- <img src=" {{ asset('images/twitter.png')}} " /> -->
        <i class="fa fa-twitter"></i>
      </a>
      <a href="https://www.instagram.com/RemirTrabalho/" target="_blank" class="social-link">
        <i class="fa fa-instagram"></i>
      </a>
    </div>
  </div>
</header>