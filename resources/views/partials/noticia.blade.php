<article class="story">
  <a href="{{$noticia->permalink}}">
    @if($noticia->video_destaque != "" && strpos($noticia->video_destaque, 'https') !== false)
      <a href="{{$noticia->permalink}}">
        <iframe class="video" width="100%" height="300" src="{{ $noticia->video_destaque}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </a>
    @else
      @if(!is_null($noticia->imagem()))
      <img class="img" src="{{ $noticia->imagem()->cloudnary->secure_url }}" />
      @endif
    @endif
  </a>
  <div class="meta">
    <span class="category"> {{ $noticia->categoria->nome}} </span>
      | por:
    <span class="author"> <a href="#"> {{ $noticia->autor }}</a> </span>
  </div>

  <h2 class="title">
    <a href="{{$noticia->permalink}}"> {{ $noticia->titulo }} </a>
  </h2>
</article>