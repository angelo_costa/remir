<footer id="main-footer" class="main-footer desktop">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="mb-2"><span class="label">Sigam-nos</span></div>
        <nav class="social-links">
          <a class="social-link" target="_blank" href="https://fb.me/RemirTrabalho"><i class="fa fa-facebook"></i></a>
          <a class="social-link" target="_blank" href="https://twitter.com/RemirTrabalho"><i class="fa fa-twitter"></i></a>
          <a class="social-link" target="_blank" href="https://www.instagram.com/RemirTrabalho/"><i class="fa fa-instagram"></i></a>
        </nav>
      </div>
      <div class="col-md-3">
        <nav class="d-flex flex-column">
          @foreach($menu as $item)
            <a href="{{ $item->permalink }}">{{ $item->titulo }}</a>
          @endforeach
        </nav>
      </div>
      <div class="col-md-3">
        <nav class="d-flex flex-column">
          <a href="/equipe">Equipe</a>
        </nav>
        <nav class="d-flex flex-column">
          <a href="/a-remir">A REMIR</a>
        </nav>
      </div>
    </div>
  </div>

</footer>

<footer class="main-footer mobile">
  <nav class="nav-mobile">
    @foreach($menu as $item)
      <a href="{{ $item->permalink }}">{{ $item->titulo }}</a>
    @endforeach
    <a href="/equipe">Equipe</a>
    <a href="/a-remir">A REMIR</a>

    <nav class="social-links">
      <a class="social-link" href="#"><i class="fa fa-facebook"></i></a>
      <a class="social-link" href="#"><i class="fa fa-twitter"></i></a>
      <a class="social-link" href="#"><i class="fa fa-instagram"></i></a>
    </nav>
  </nav>
</footer>