@extends('layouts.base', [ "menu" => $menu ])

@section('meta')
<title>{{ $noticia->titulo }}</title>
<meta property="og:title" content="{{ $noticia->titulo }}">
<meta property="og:site_name" content="REMIR Trabalho">
<meta property="og:url" content="{{ $noticia->permalink }}">
<meta property="og:description" content="{{ $noticia->subtitulo }}">

@if(null !== $noticia->imagem())
<meta property="og:image" content="{{ $noticia->imagem()->cloudnary->secure_url }}">
@endif

@endsection
@section('css')
<style>
  @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
</style>
<link type="text/css" rel="stylesheet" href="{{ URL::asset('css/jssocials.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ URL::asset('css/jssocials-theme-plain.css') }}" />
@endsection

@section('js')
<script type="text/javascript" src="{{ URL::asset('js/jssocials.min.js') }}"" ></script>
<script type="text/javascript" src="{{ URL::asset('js/news-page-share-buttons.js') }}"></script>
@endsection

@section('content')

<div class="container small">
<div class="jssocial desktop side rounded news-show" id="desktop-share-buttons"></div>
  <article class="news news-show">
    <h3 class="mt-5 mb-1 category">
      <a href="{{ $noticia->categoria->permalink }}">
        {{ $noticia->categoria->nome }}
      </a>
    </h3>
    <h1 class="my-4 title">
      {{$noticia->titulo}}
    </h1>

    <div class="meta {{ $noticia->imagem() != null ? 'overflow' : '' }} ">
      <span class="published_at">{{ \Carbon\Carbon::parse($noticia->created_at)->formatLocalized('%d de %B de %Y') }} </span>
      <span class="author">Autoria: <a href="/pesquisa?q={{ $noticia->autor }}&como=autor">{{ $noticia->autor }}</a></span>
    </div>

    @if($noticia->video_destaque != '')
    <div class="img-wrapper">
      <iframe class="mb-3 img-highlight" width="100%" height="460" src="{{ $noticia->video_destaque}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    @else
      @if(null !== $noticia->imagem())
      <div class="img-wrapper">
        <img id="news-img-highlight" class="img-highlight" src=" {{ $noticia->imagem()->cloudnary->secure_url }} "/>
      </div>
      @endif
    @endif

    @if(!empty($noticia->subtitulo))
      <h3 class="subtitle mb-3">{{ $noticia->subtitulo }} </h3>
    @endif

    <section id="news-body" class="body">
      {!! str_replace('<p><br></p>', '', $noticia->corpo) !!}
      <div class="jssocial mobile inline rounded news-show" id="mobile-share-buttons"></div>
    </section>
  </article>

  <section class="tags my-5">
    <h3 class="label mb-4">TAGS</h3>
    <ul class="tags-list list list-unstyled">

      @foreach($noticia->tags as $tag)
        <li class="tag">
          <a href="{{ $tag->permalink }}"> {{ $tag->nome }} </a>
        </li>
      @endforeach
    </ul>
  </section>

  @if(count($noticia->relacionados(2)) > 0)
  <section class="more my-5">
    <h3 class="label mb-4">LEIA MAIS</h3>
      <div class="row">
        @foreach($noticia->relacionados(2) as $noticia_relacionada)
          <article class="col-sm story mb-5 mb-md-0 mb-lg-0 mb-xl-0">
              @if(null !== $noticia_relacionada->imagem())
              <a href="{{ $noticia_relacionada->permalink }}">
                <img class="img" src=" {{ $noticia_relacionada->imagem()->cloudnary->secure_url }} "/>
              </a>
              @endif
              <div class="meta">
                <span class="category"> {{ $noticia_relacionada->categoria->nome}} </span>
                | por:
                <span class="author"> <a href="#"> REMIR</a> </span>
              </div>
              <h2 class="title">
                <a  href="{{ $noticia_relacionada->permalink }}"> {{ $noticia_relacionada->titulo }} </a>
              </h2>
          </article>
        @endforeach
      </div>
    </ul>
  </section>
  @endif
</div>


@endsection
