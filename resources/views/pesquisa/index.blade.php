@extends('layouts.base', [ "menu" => $menu])

@section('css')
<style>
  @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
</style>
@endsection

@section('js')
@endsection

@section('content')
<div class="container">
  <!-- Nennhum resultado. -->
  @if(count($noticias) < 1 && count($tags) < 1 && count($livros) < 1 && count($categorias) < 1)
    <h2 class="title my-5"> Nenhum resultado para <em>"{{ $query }}"</em>
    </h2>
  @endif

  <!-- Encontrou resultados. -->
  @if(count($noticias) > 0 || count($tags) > 0 || count($livros) > 0 || count($categorias) > 0)
    <h2>Encontramos {{ count($noticias) + count($tags) + count($categorias) + count($livros) }}
      resultados para <em>"{{ $query }}"</em>
    </h2>
  @endif

  @if(count($noticias) > 0)
    <section class="tags my-5">
      <h3 class="label mb-4">Notícias</h3>

      @foreach($noticias->chunk(4) as $chunk)
        <div class="row">
          @foreach($chunk as $noticia)
          <div class="col-md-3 col-lg-3 mb-5 mb-md-5 mb-lg-5">
            @include('partials.noticia', [ 'noticia' => $noticia ])
          </div>
          @endforeach
        </div>
      @endforeach
    </section>
  @endif

  @if(count($categorias) > 0)
    <section class="tags my-5">
      <h3 class="label mb-4">Categorias</h3>
      <ul class="tags-list list list-unstyled">

        @foreach($categorias as $categoria)
          <li class="tag">
            <a href="{{ $categoria->permalink }}"> {{ $categoria->nome }} </a>
          </li>
        @endforeach
      </ul>
    </section>
  @endif

  @if(count($tags) > 0)
    <section class="tags my-5">
      <h3 class="label mb-4">Tags</h3>
      <ul class="tags-list list list-unstyled">

        @foreach($tags as $tag)
          <li class="tag">
            <a href="{{ $tag->permalink }}"> {{ $tag->nome }} </a>
          </li>
        @endforeach
      </ul>
    </section>
  @endif

  @if(count($livros) > 0)
    <section class="tags my-5">
      <h3 class="label mb-4">Publicações</h3>
      @foreach($livros as $livro)
      <div class="row mb-5">
        <div class="col-md-12">
          <article class="book">
            <div class="meta top mb-2">
              <span class="tipo">
                {{ $livro->tipo }} 
              </span>
            </div>
            <h2 class="title">
              <a href="{{ $livro->permalink }}">
                {{ $livro->titulo }}
              </a>
            </h2>
            <div class="meta bottom">
              <span class="author"> {{ $livro->autor }} </span>
              <span class="year"> {{ $livro->ano }} </span>
              <span class="institution"> {{ $livro->sigla }} </span>
            </div>
          </article>
        </div>
      </div>
      @endforeach
    </section>
  @endif
</div>
@endsection
