$(document).ready(function() {

  $("#mobile-share-buttons").jsSocials({
    shareIn: "popup",
    showLabel: false,
    showCount: false,
    shares: ["facebook", "twitter", "email", "whatsapp"]
  });

  $("#desktop-share-buttons").jsSocials({
    shareIn: "popup",
    showLabel: false,
    showCount: false,
    shares: ["facebook", "twitter", "email", "whatsapp"]
  });

  const title = document.querySelector('.title');
  const bounds = title.getBoundingClientRect();


  $("#desktop-share-buttons")
  .css('position', 'fixed')
  .css('top', (bounds.top + window.scrollY) + 'px')
  .css('left', (bounds.left - 90) + 'px')
  .show();


  inView.offset(0)
  inView('#main-footer')
    .on('enter', function() {
      $("#desktop-share-buttons").hide();
    })
    .on('exit', function() {
      $("#desktop-share-buttons").show();
    })
});


