$(document).ready(function() {

  const map = L.map('map');
  const markers = L.markerClusterGroup();

  map.setView([-15.77972, -47.92972], 4);

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZmRlbGltYSIsImEiOiJjanV4bHQxZ3kwb24yNDVqd29odW83cXhxIn0.Fu78Xk0C-roAJHJpIprHNQ', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'your.mapbox.access.token'
  }).addTo(map);

  axios.get('/equipe/dados')
    .then(response => {
      const membros = response.data;

      membros.forEach(m => {
        const latLng = [ parseFloat(m.lat), parseFloat(m.lng)];

        let icon = {};
        let marker = {};

        if (m.imagem) {
          icon = L.icon({
            iconUrl: m.imagem.cloudnary.secure_url,
            className: 'custom-icon',
            iconSize:     [45, 45],
          });

          marker = L.marker(latLng, { icon: icon });
        } else {
          marker = L.marker(latLng);
        }

        markers.addLayer(marker);
      });
      map.addLayer(markers);
    });
});
