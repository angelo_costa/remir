$(document).ready(function() {

  $container = $('.tag-cloud-container .tags-cloud');

  axios.get('/api/tag/nuvem')
    .then(response => {
      const tags = response.data;
      const max = tags[0].noticias_count;
      const min = tags[tags.length - 1].noticias_count;

      const orderedTags = tags.sort((a, b) => {
        if(a.nome < b.nome) { return -1; }
        if(a.nome > b.nome) { return 1; }
        return 0;
      });

      orderedTags.forEach(tag => {
        const variance = (tag.noticias_count - min) / (max - min);
        const fsize = 1 + variance + 'em';

        $link = $('<a>')
        $link.text(tag.nome);
        $link.addClass('tag-link');
        $link.attr('href', tag.permalink);
        $link.css('font-size', fsize);
        $container.append($link);
        // console.log($link);

      })
      // console.log(r);
    })

})