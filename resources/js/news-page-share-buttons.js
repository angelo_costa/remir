$(document).ready(function() {

  $("#mobile-share-buttons").jsSocials({
    shareIn: "popup",
    showLabel: false,
    showCount: false,
    shares: ["facebook", "twitter", "email", "whatsapp"]
  });

  $("#desktop-share-buttons").jsSocials({
    shareIn: "popup",
    showLabel: false,
    showCount: false,
    shares: ["facebook", "twitter", "email", "whatsapp"]
  });

  const newsImgHighlight = document.getElementById('news-img-highlight');
  const bounds = newsImgHighlight.getBoundingClientRect();


  $("#desktop-share-buttons")
  .css('position', 'fixed')
  .css('top', (bounds.top + window.scrollY - 5) + 'px')
  .css('left', (bounds.left - 50) + 'px')
  .show();


  inView.offset(300);
  inView('#news-body').on('enter', function() {
    $("#desktop-share-buttons")
      .css('position', 'fixed')
      .css('top', 'calc(50vh - 100px)')
  });

  inView.offset(0);
  inView('#news-img-highlight').on('enter', function() {
      const newsImgHighlight = document.getElementById('news-img-highlight');
      const bounds = newsImgHighlight.getBoundingClientRect();

      $("#desktop-share-buttons")
      .css('top', (bounds.top + window.scrollY - 5) + 'px')
      .css('left', (bounds.left - 50) + 'px')
  });

  inView.offset(0)
  inView('#main-footer')
    .on('enter', function() {
      $("#desktop-share-buttons").hide();
    })
    .on('exit', function() {
      $("#desktop-share-buttons").show();
    })
});


