<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Categoria;

class CategoriaTest extends TestCase
{
    use RefreshDatabase;

    protected function generateData(string $nome = NULL) {
        if ($nome) {
            return factory(Categoria::class)->create([
                "nome" => $nome,
            ]);
        }
        else {
            return factory(Categoria::class)->create();
        }
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testGetAll() {
        $categorias = [
            'Economia',
            'Mundo',
            'Saúde',
            'Tecnologia',
            'Ciência',
        ];
    
        for ($i = 0; $i < 5; $i++) {
            $this->generateData($categorias[$i]);
        }

        $response = $this->json('GET', 'api/categoria/all');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    0 => [
                        'nome',
                        'descricao',
                    ],
                ],
                "links" => [
                    "first",
                    "last",
                    "prev",
                    "next" 
                ],
                "meta" => [
                    "current_page",
                    "from",
                    "last_page",
                    "path",
                    "per_page",
                    "to",
                    "total",
                ],
            ]);
    }

    public function testCreateCategoria() {
        $parameters = [
            'nome' => 'Brasil',
            'descricao' => 'O Brasil é um país de muitas belezas naturais.'
        ];

        $response = $this->json('POST', 'api/categoria/create', $parameters);

        $response
            ->assertStatus(201)
            ->assertJson([
                'nome' => 'Brasil',
                'descricao' => 'O Brasil é um país de muitas belezas naturais.'
            ]);

    }

    public function testCreateCategoriaWithErrors() {
        $parameters = [];

        $response = $this->json('POST', 'api/categoria/create', $parameters);

        $response
            ->assertStatus(401)
            ->assertJson([
                'nome' => [
                    'The nome field is required.'
                ],
            ]);

    }

    public function testFindCategoria() {
        $categorias = [
            'Economia',
            'Mundo',
            'Saúde',
            'Tecnologia',
            'Ciência',
        ];

        $categoriasFake = [];
    
        for ($i = 0; $i < 5; $i++) {
            $categoriasFake[] = $this->generateData($categorias[$i]);
        }

        $response = $this->json('GET', "api/categoria/find/{$categoriasFake[0]->id}");

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'nome' => 'Economia',
            ]);

    }

    public function testUpdateCategoria() {
        $categorias = [
            'Economia',
            'Mundo',
            'Saúde',
            'Tecnologia',
            'Ciência',
        ];

        $parameters = [
            'nome' => 'Economia do Brasil',
            'descricao' => 'Economia do Brasil é um objeto de estudo.'
        ];

        $categoriasFake = [];
    
        for ($i = 0; $i < 5; $i++) {
            $categoriasFake[] = $this->generateData($categorias[$i]);
        }

        $response = $this->json('POST', "api/categoria/update/{$categoriasFake[0]->id}", $parameters);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'nome' => 'Economia do Brasil',
                'descricao' => 'Economia do Brasil é um objeto de estudo.',
            ]);

    }

    public function testDeleteCategoria() {
        $categorias = [
            'Economia',
            'Mundo',
            'Saúde',
            'Tecnologia',
            'Ciência',
        ];

        $categoriasFake = [];
    
        for ($i = 0; $i < 5; $i++) {
            $categoriasFake[] = $this->generateData($categorias[$i]);
        }

        $response = $this->json('POST', "api/categoria/delete/{$categoriasFake[0]->id}");

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'Category deleted witth success',
            ])
            ->assertJsonMissingExact([
                'nome' => 'Economia'
            ]);

    }
}
