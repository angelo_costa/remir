/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 43);
/******/ })
/************************************************************************/
/******/ ({

/***/ 43:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(44);


/***/ }),

/***/ 44:
/***/ (function(module, exports) {

$(document).ready(function () {

  $("#mobile-share-buttons").jsSocials({
    shareIn: "popup",
    showLabel: false,
    showCount: false,
    shares: ["facebook", "twitter", "email", "whatsapp"]
  });

  $("#desktop-share-buttons").jsSocials({
    shareIn: "popup",
    showLabel: false,
    showCount: false,
    shares: ["facebook", "twitter", "email", "whatsapp"]
  });

  var newsImgHighlight = document.getElementById('news-img-highlight');
  var bounds = newsImgHighlight.getBoundingClientRect();

  $("#desktop-share-buttons").css('position', 'fixed').css('top', bounds.top + window.scrollY - 5 + 'px').css('left', bounds.left - 50 + 'px').show();

  inView.offset(300);
  inView('#news-body').on('enter', function () {
    $("#desktop-share-buttons").css('position', 'fixed').css('top', 'calc(50vh - 100px)');
  });

  inView.offset(0);
  inView('#news-img-highlight').on('enter', function () {
    var newsImgHighlight = document.getElementById('news-img-highlight');
    var bounds = newsImgHighlight.getBoundingClientRect();

    $("#desktop-share-buttons").css('top', bounds.top + window.scrollY - 5 + 'px').css('left', bounds.left - 50 + 'px');
  });

  inView.offset(0);
  inView('#main-footer').on('enter', function () {
    $("#desktop-share-buttons").hide();
  }).on('exit', function () {
    $("#desktop-share-buttons").show();
  });
});

/***/ })

/******/ });