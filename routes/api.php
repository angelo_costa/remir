<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\\Auth\\AuthController@login');
    Route::post('signup', 'Api\\Auth\\AuthController@signup');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Api\\Auth\\AuthController@logout');
        Route::get('user', 'Api\\Auth\\AuthController@user');
    });
});

Route::group([
    'prefix' => 'password'
], function () {
    Route::post('create', 'Api\\Password\\PasswordResetController@create');
    Route::get('find/{token}', 'Api\\Password\\PasswordResetController@find');
    Route::post('reset', 'Api\\Password\\PasswordResetController@reset');
});

Route::group([
  'prefix' => 'equipe'
], function () {
  Route::get('all', 'Api\\Equipe\\EquipeController@all');
  Route::get('find/{id}', 'Api\\Equipe\\EquipeController@find');

  Route::group([
    // 'middleware' => 'auth:api'
  ], function() {
    Route::post('create', 'Api\\Equipe\\EquipeController@create');
    Route::post('update/{id}', 'Api\\Equipe\\EquipeController@update');
    Route::post('delete/{id}', 'Api\\Equipe\\EquipeController@delete');
  });
});



Route::group([
  'prefix' => 'a-remir'
], function () {
  Route::get('all', 'Api\\Remir\\RemirController@all');
  Route::post('create', 'Api\\Remir\\RemirController@create');
  Route::post('update/{id}', 'Api\\Remir\\RemirController@update');
  Route::post('delete/{id}', 'Api\\Remir\\RemirController@delete');
  Route::get('find/', 'Api\\Remir\\RemirController@find');
});

Route::group([
    'prefix' => 'convite'
], function () {
    Route::post('create', 'Api\\Convite\\ConviteController@create');
    Route::get('find/{token}', 'Api\\Convite\\ConviteController@find');
});

Route::group([
    'prefix' => 'auth/admin'
], function () {
    Route::post('login', 'Api\\Admin\\AdminController@login');
    Route::post('signup', 'Api\\Admin\\AdminController@signup');

    Route::group([
        'middleware' => 'auth:admin-api'
    ], function() {
        Route::get('logout', 'Api\\Admin\\AdminController@logout');
        Route::get('user', 'Api\\Admin\\AdminController@user');
    });
});

Route::group([
  'prefix' => 'user'
], function () {
  Route::get('all', 'Api\\User\\UserController@all');
  Route::post('create/{token}', 'Api\\User\\UserController@create');

})
;
Route::group([
    'prefix' => 'categoria'
], function () {
    Route::get('all', 'Api\\Categoria\\CategoriaController@all');
    Route::get('find/{categoriaId}', 'Api\\Categoria\\CategoriaController@find');

    Route::group([
        // 'middleware' => 'auth:api'
    ], function() {
        Route::post('{categoriaId}/destaques','Api\\Categoria\\CategoriaController@saveDestaques' );
        Route::post('create',                 'Api\\Categoria\\CategoriaController@create');
        Route::post('update/{categoriaId}',   'Api\\Categoria\\CategoriaController@update');
        Route::post('delete/{categoriaId}',   'Api\\Categoria\\CategoriaController@delete');
    });
});

Route::group([
  'prefix' => 'livro'
], function () {
  Route::get('all', 'Api\\Livro\\LivroController@all');
  Route::get('tipos', 'Api\\Livro\\LivroController@tipos');
  Route::get('find/{livroId}', 'Api\\Livro\\LivroController@find');

  Route::group([
      // 'middleware' => 'auth:api'
  ], function() {
      Route::post('create',             'Api\\Livro\\LivroController@create');
      Route::post('update/{livroId}',   'Api\\Livro\\LivroController@update');
      Route::post('delete/{livroId}',   'Api\\Livro\\LivroController@delete');
  });
});

Route::group([
  'prefix' => 'dado'
], function () {
  Route::get('all', 'Api\\Dado\\DadoController@all');
  Route::get('find/{dadoId}', 'Api\\Dado\\DadoController@find');

  Route::group([
      // 'middleware' => 'auth:api'
  ], function() {
      Route::post('create',             'Api\\Dado\\DadoController@create');
      Route::post('update/{dadoId}',   'Api\\Dado\\DadoController@update');
      Route::post('delete/{dadoId}',   'Api\\Dado\\DadoController@delete');
  });
});

Route::group([
  'prefix' => 'tag'
], function () {
  Route::get('all',          'Api\\Tag\\TagController@all');
  Route::get('find/{tagId}', 'Api\\Tag\\TagController@find');

  Route::group([
      // 'middleware' => 'auth:api'
  ], function() {
      Route::post('create',           'Api\\Tag\\TagController@create');
      Route::post('update/{tagId}',   'Api\\Tag\\TagController@update');
      Route::post('delete/{tagId}',   'Api\\Tag\\TagController@delete');
  });
});

Route::group([
    'prefix' => 'menu',
], function () {
    Route::get('all', 'Api\\Menu\\MenuController@all');
    Route::get('find/{menuId}', 'Api\\Menu\\MenuController@find');

    Route::group([
      // 'middleware' => 'auth:api'
    ], function() {
      Route::post('create', 'Api\\Menu\\MenuController@create');
      Route::post('update/{menuId}', 'Api\\Menu\\MenuController@update');
      Route::post('updateOrder', 'Api\\Menu\\MenuController@updateOrder');
      Route::post('delete/{menuId}', 'Api\\Menu\\MenuController@delete');
    });
});

Route::group([
    'prefix' => 'noticia-status',
], function () {
    Route::get('all', 'Api\\Noticia\\NoticiaStatusController@all');
});

Route::group([
    'prefix' => 'noticia',
], function () {
    Route::get('all', 'Api\\Noticia\\NoticiaController@all');
    Route::get('find/{noticiaId}', 'Api\\Noticia\\NoticiaController@find');
    Route::post('create', 'Api\\Noticia\\NoticiaController@create');
    Route::post('update/{noticiaId}', 'Api\\Noticia\\NoticiaController@update');
    Route::post('delete/{noticiaId}', 'Api\\Noticia\\NoticiaController@delete');
});

Route::group([
  'prefix' => 'midia',
], function () {
  Route::get('all', 'Api\\Midia\\MidiaController@all');
  Route::get('find/{midiaId}', 'Api\\Midia\\MidiaController@find');

  Route::group([
    // 'middleware' => 'auth:api'
  ], function() {
    Route::post('create', 'Api\\Midia\\MidiaController@create');
    Route::post('update/{midiaId}', 'Api\\Midia\\MidiaController@update');
    Route::post('delete/{midiaId}', 'Api\\Midia\\MidiaController@delete');
  });
});

Route::group([
    'prefix' => 'chamada',
], function () {
    Route::get('all', 'Api\\Chamada\\ChamadaController@all');
    Route::get('find/{chamadaId}', 'Api\\Chamada\\ChamadaController@find');

    Route::group([
      // 'middleware' => 'auth:api'
    ], function() {
      Route::post('create', 'Api\\Chamada\\ChamadaController@create');
      Route::post('update/{chamadaId}', 'Api\\Chamada\\ChamadaController@update');
      Route::post('delete/{chamadaId}', 'Api\\Chamada\\ChamadaController@delete');
    });
});


Route::group([
    'prefix' => 'secao',
], function () {
    Route::get('all', 'Api\\Secao\\SecaoController@all');
    Route::get('find/{secaoId}', 'Api\\Secao\\SecaoController@find');

    Route::group([
      // 'middleware' => 'auth:api'
    ], function() {
      Route::post('create', 'Api\\Secao\\SecaoController@create');
      Route::post('update/{secaoId}', 'Api\\Secao\\SecaoController@update');
      Route::post('delete/{secaoId}', 'Api\\Secao\\SecaoController@delete');
    });
});

Route::group([
    'prefix' => 'tag',
], function () {
    Route::get('nuvem', 'Api\\Tag\\TagController@nuvem');
    Route::get('all', 'Api\\Tag\\TagController@all');
    Route::get('find/{tagId}', 'Api\\Tag\\TagController@find');

    Route::group([
      // 'middleware' => 'auth:api'
    ], function() {
      Route::post('create', 'Api\\Tag\\TagController@create');
      Route::post('{tagId}/update', 'Api\\Tag\\TagController@update');
      Route::post('{tagId}/delete', 'Api\\Tag\\TagController@delete');
    });
});

Route::group([
  'prefix' => 'home',
], function () {
  Route::get('/', 'Api\\Home\\HomeController@find');

  Route::group([
    // 'middleware' => 'auth:api'
  ], function() {
    Route::post('create', 'Api\\Home\\HomeController@create');
  });
});
