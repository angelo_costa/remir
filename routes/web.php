<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => 'categoria'
], function () {
    Route::get('all', 'Web\\Categoria\\CategoriaController@all')->middleware('guest');
    Route::get('{permalink}', 'Web\\Categoria\\CategoriaController@show')->middleware('guest');;
});

Route::group([
  'prefix' => 'tag'
], function () {
  Route::get('{permalink}', 'Web\\Tag\\TagController@show')->middleware('guest');;
});

Route::group([
    'prefix' => 'noticia',
], function () {
    Route::get('all', 'Web\\Noticia\\NoticiaController@all');
    Route::get('{permalink}', 'Web\\Noticia\\NoticiaController@show');
});

Route::group([
  'prefix' => 'biblioteca',
], function () {
  Route::get('/', 'Web\\Livro\\LivroController@all');
  Route::get('{permalink}', 'Web\\Livro\\LivroController@show');
});


Route::group([
  'prefix' => 'dados',
], function () {
  Route::get('/', 'Web\\Dado\\DadoController@all');
});

Route::get('/equipe', 'Web\\Equipe\\EquipeController@all');
Route::get('/a-remir', 'Web\\Remir\\RemirController@show');
Route::get('/equipe/dados', 'Web\\Equipe\\EquipeController@dados');


Route::get('/', 'Web\\Home\\HomeController@show');


Route::group([
  'prefix' => 'pesquisa',
], function () {
  Route::get('/', 'Web\\Search\\SearchController@search');
});


Auth::routes();

